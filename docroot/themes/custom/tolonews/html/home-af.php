<!DOCTYPE html>
<html lang="en">
    <head>
        <title>طلوع نیوز</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!--<link rel="stylesheet" type="text/css" href="css/common.css">-->
        <!--<link rel="stylesheet" type="text/css" href="css/home.css">-->
        <!--<link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <link rel="stylesheet" href="css/rtl-app.css">
    </head>
    <body class="is-exiting">
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header-af.php'; ?>
        <div id="wrapper" class="wrapper clearfix">
            <!--BLOCKS LAYOUT ONLY MOBILE->-->
            <div class="visible-onlymobile">
                <!--WEATHER MOBILE ONLY-->
                <div class="box-weather twoo-column-tablets last">
                    <div class="container-weather">
                        <div class="image-weather"><img src="images/image-meteo.jpg" alt="" width="310" height="289" /></div>
                        <a href="http://www.accuweather.com/fa/af/kabul/4361/weather-forecast/4361" class="aw-widget-legal">
                            <!--
                                By accessing and/or using this code snippet, you agree to AccuWeather's terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather's Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                                -->
                        </a>
                        <div id="awcc1455525954273" class="aw-widget-current"  data-locationkey="4361" data-unit="c" data-language="fa" data-useip="false" data-uid="awcc1455525954273"></div>
                        <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                    </div>
                </div>
                <!--END WEATHER MOBILE ONLY-->
                <!--TOP ARTICLE TOLONEWS MOBILE ONLY-->
                <article class="top-post-tolonews clearfix">
                    <a href="#" title=""> <img src="images/img1.jpg" alt="" width="435" height="289" /> </a>
                    <div class="content-top-post-tolonews">
                        <h2 class="grey-dark1 title-top-post-tolonews">
                            <a href="#" title="">عراق را رمادی در روز</a>
                        </h2>
                        <div class="part-left full-width-col-mobile grey-dark4">
                            <p>حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است.</p>
                            <span class="post-date grey-light3">
                            <span class="uppercase-txt">امروز</span> - 03:15 </span>
                        </div>
                    </div>
                </article>
                <!--TOP ARTICLE TOLONEWS MOBILE ONLY-->
            </div>
            <!--END BLOCKS ONLY MOBILE-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <article class="top-post-tolonews hidden-onmobile clearfix">
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                <a href="#" title="">عراق را رمادی در روز</a>
                            </h2>
                            <div class="part-left full-width-col-mobile grey-dark4">
                                <p>حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است.</p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                <div class="related-news hidden-onmobile">
                                    <h3 class="red uppercase-txt">اخبار مرتبط</h3>
                                    <ul>
                                        <li> <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                            <i class="fa fa-angle-right red"></i>
                                            </a>
                                        </li>
                                        <li> <a href="#" title="">نمایندگان مجلس پاسخ تقاضا در شایعات از کمک های دولتی در داعش دست
                                            <i class="fa fa-angle-right red"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="part-right full-width-col-mobile">
                                <a href="#" title=""> <img src="images/img1.jpg" alt="" width="435" height="289" /> </a>
                            </div>
                        </article>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <!--DEBUT FEATURED NEWS-->
                        <div class="featured clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category  uppercase-txt">خبرهای وِیژه</h2>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a> </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا </a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a> </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                            <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="بارگیری بیشتر">بارگیری بیشتر</a> </div>
                        </div>
                        <!--END FEATURED NEWS-->
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col">
                        <!--TOP STORIES-->
                        <div class="equal-top-first list-item-widget clearfix bar-bottom hidden-ontablets hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">خبرهای مهم</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END TOP STORIES-->
                        <!--6PM BULLETIN VIDEO-->
                        <div class="equal-top box-video hidden-onmobile twoo-column-tablets">
                            <h3 class="line line-grey-dark grey-dark1 uppercase-txt">اخبار ساعت ۶</h3>
                            <div class="box-medium-video grey-box clearfix bar-bottom">
                                <div class="video">
                                    <a href="#" title="video">
                                        <img src="images/video.jpg" alt="" width="280" height="187">
                                        <div class="play"> <i class="medium fa fa-play"></i> </div>
                                    </a>
                                </div>
                                <h3 class="title">دوشنبه ۲۲ می </h3>
                                <div class="text-vds">به کارگردانی جیمز دوپونت
                                    <br> غنی ابراز اعتماد به نفس در داعش
                                </div>
                            </div>
                        </div>
                        <!--END 6PM BULLETIN VIDEO-->
                        <!--PUB-->
                        <div class="box grey-box equal-top-pub twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                    </div>
                </div>
            </div>
            <!--WATCH-->
            <div class="row grey-dark marginRow">
                <div class="inner-container row-watch-tolo">
                    <div class="container-top-post line line-grey clearfix">
                        <h2 class="title-category  uppercase-txt">ببینید</h2>
                        <a href="#" title="تماشای تمام ویدیوها" class="view-all-post grey-light5">تماشای تمام ویدیوها</a>
                    </div>
                    <div class="two-col full-width-col-tablets hidden-onmobile content-watch-tolo">
                        <div class="container-video-cover">
                            <div class="video-hidden hidden">
                                <iframe width="650" height="365" src="https://www.youtube.com/embed/2L_WTN4OOqc" allowfullscreen=""></iframe>
                            </div>
                            <div class="block-video-play video">
                                <!--video watch-->
                                <img src="images/img8.jpg" alt="" width="650" height="365" />
                                <i class="big-play fa fa-play play"></i>
                            </div>
                        </div>
                        <div class="info">
                            <h3><a href="#" title="مرسی مصر زندانی به مدت 20 سال">مرسی مصر زندانی به مدت 20 سال</a></h3>
                            <span class="post-date grey-light3">
                            <span class="uppercase-txt">امروز</span> - 03:15 </span>
                        </div>
                    </div>
                    <div class="one-col last-col full-width-col-tablets thumbs-watch-tolo full-width-col-mobile">
                        <h3 class="uppercase-txt hidden-onmobile">ویدیوهای مهم</h3>
                        <ul>
                            <li class="post-tolonews video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>کنر هواپیماهای بدون سرنشین اعتصاب کشته چهار طالبان</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds1.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>اوباما می گوید آمریکا علیه داعش از دست دادن ندارد</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews  hidden-onmobile video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>کنر هواپیماهای بدون سرنشین اعتصاب کشته چهار طالبان</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews hidden-onmobile video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds1.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>اوباما می گوید آمریکا علیه داعش از دست دادن ندارد</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END WATCH-->
            <div class="row">
                <div class="inner-container">
                    <div class="one-col full-width-col-tablets hidden-onmobile">
                        <!--MOST READ / MOST WATCHED TABULATION-->
                        <div class="list-item-widget twoo-column-tablets bar-bottom">
                            <ul class="tabulations-tolo container-top-post no-margin line-red red">
                                <li class="titleBlock uppercase-txt"> <a class="active" href="#most-read">بیشتر خوانده شده‌ها/ پر بیننده‌ترین‌ها</a> </li>
                                <li class="grey-light5 uppercase-txt"> <a href="#most-watched">پر بازدیدترین‌ها</a> </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-read">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-watched">
                                <li>
                                    <a href="#"> <span class="number grey-light3">6</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">7</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">8</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">9</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">10</span> <span class="content">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END MOST READ / MOST WATCHED TABULATION-->
                        <!--WEATHER-->
                        <div class="box-weather twoo-column-tablets last">
                            <h3 class="line bold uppercase-txt">آب و هوا</h3>
                            <div class="container-weather">
                                <div class="image-weather"><img src="images/image-meteo.jpg" alt="" width="310" height="289" /></div>
                                <a href="http://www.accuweather.com/fa/af/kabul/4361/weather-forecast/4361" class="aw-widget-legal">
                                    <!--
                                        By accessing and/or using this code snippet, you agree to AccuWeather's terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather's Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                                        -->
                                </a>
                                <div id="awcc1455525954273" class="aw-widget-current"  data-locationkey="4361" data-unit="c" data-language="fa" data-useip="false" data-uid="awcc1455525954273"></div>
                                <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                            </div>
                        </div>
                        <!--END WEATHER-->
                    </div>
                    <!--BUSINESS-->
                    <div class="two-col last-col full-width-col-tablets full-width-col-mobile">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">بازرگانی</h2>
                            <a title="تمام خبرهای بازرگانی" href="#" class="view-all-post grey-light5">تمام خبرهای بازرگانی</a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img22.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">مرکل، اولاند به یونان به نگاهی مسیر به توافقنامه</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img33.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">میلیون ها بشکه نفت در مورد به صفر</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img44.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">ژاپن هنوز ضرب و شتم چین در یکی امتیاز: بستانکار برتر جهان</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img55.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">چندمنظوره در حال اجرا داغ دریایی شیل فلس برگشت</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img66.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">عراق حیدر العبادی: چارلی رز</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image"> <img src="images/img77.png" alt="" width="147" height="98"> </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title=""> سود افزایش با وجود تاثیر جنگ عراق</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="بارگیری بیشتر">بارگیری بیشتر</a> </div>
                    </div>
                    <!--END BUSINESS-->
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="one-col last-col full-width-col-tablets right-col full-width-col-mobile">
                        <!--CURRENCY-->
                        <div class="currency bar-bottom twoo-column-tablets hidden-onmobile">
                            <?php
                                $url['USD'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNUSD&f=USD&v=1&do=cvals&ln=fr';
                                $url['EUR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNEUR&f=EUR&v=1&do=cvals&ln=fr';
                                $url['INR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNINR&f=INR&v=1&do=cvals&ln=fr';
                                $url['PKR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNPKR&f=PKR&v=1&do=cvals&ln=fr';
                                $url['IRR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNIRR&f=IRR&v=1&do=cvals&ln=fr';
                                $url['AED'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNAED&f=AED&v=1&do=cvals&ln=fr';

                                setlocale(LC_MONETARY, 'en_US');
                                foreach($url as $currency => $link){
                                	$obj = json_decode(file_get_contents($link), true);
                                	$Res[$currency]= $obj["AFN"];
                                }
                                // print_r($Res);
                                ?>
                            <div class="container-top-post no-margin line line-red red clearfix">
                                <h3 class="titleBlock uppercase-txt">نرخ ارز</h3>
                                <span class="grey-light5 view-all-post">آخرین خبر: 05.20.15</span>
                            </div>
                            <table class="grey-box">
                                <tbody>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-us"></td>
                                        <td class="symbol">1 USD</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['USD']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-eu"></td>
                                        <td class="symbol">1 Euro</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['EUR']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-in"></td>
                                        <td class="symbol">1000 INR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['INR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-pk"></td>
                                        <td class="symbol">1000 PKR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['PKR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ir"></td>
                                        <td class="symbol">1000 IRR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['IRR']*1000 ); ?>AFN</td>
                                    </tr>
                                    <tr>
                                        <td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ae"></td>
                                        <td class="symbol">1 AED</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['AED']); ?>AFN</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--END CURRENCY-->
                        <!--PUB 2-->
                        <div class="box equal-top-pub pub grey-box twoo-column-tablets last">
                            <a href="#" title=""> <img src="images/pub2.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB 2-->
                    </div>
                    <!--CATEGROY SPORT-->
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">ورزش</h2>
                            <a href="#" title="دیدن اخبار ورزشی " class="view-all-post grey-light5">دیدن اخبار ورزشی </a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport1.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">تیم والیبال مگس به بنگلادش برای آسیا مسابقه</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport2.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">فوتبال افغانستان تیم میزبانی جام جهانی مقدماتی مسابقات در </a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است… </a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small1.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">قندهار فدراسیون فوتبال رئیس متهم به اختلاس</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small2.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">U16 های لیگ برتر فوتسال لگد کردن در کابل</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small3.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">هندبال دوستانه جام ضربات خاموش در کابل</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small4.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">تیم ملی فوتبال سر در خارج از کشور به آماده شدن برای جام جهانی مقدماتی</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="بارگیری بیشتر">بارگیری بیشتر</a> </div>
                        <!--END SPORT-->
                    </div>
                    <!--CATEGROY WORLD-->
                    <div class="four-col twoo-col-ontablets last-col marginRow">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">جهان</h2>
                            <a href="#" title="دیدن اخبار جهان " class="view-all-post grey-light5">دیدن اخبار جهان </a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">800 مرده پس از کشتی شکسته مهاجر مدیترانه روز یکشنبه</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="two-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world1.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">یمن شورشی رهبر عهد مقاومت در برابر عربستان به رهبری هوا</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="three-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world2.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">عربستان سعودی متعهد به صندوق کمک های اولیه برای یمن، نگه دارید تا هوا حملات</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="four-col last-col">
                                <article class="post-tolonews bar-bottom medium-column last">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">بار القاعده جنوب شرقی یمن فرودگاه</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="بارگیری بیشتر">بارگیری بیشتر</a> </div>
                    </div>
                    <!--END WORLD-->
                </div>
            </div>
            <div class="row grey-dark marginRow hidden-onmobile">
                <!--SOCIAL SHARE-->
                <div class="inner-container row-social-tolo">
                    <h2 class="uppercase-txt line line-grey2 ">ما را در شبکه های اجتماعی دنبال کنید</h2>
                    <ul class="social-media">
                        <li>
                            <a href="https://www.facebook.com/TOLOnews" target="_blank" title="فیس بوک"> <i class="fa fa-facebook"></i> فیس بوک </a>
                        </li>
                        <li>
                            <a href="http://www.twitter.com/TOLOnews" target="_blank" title="توئیتر"> <i class="fa fa-twitter"></i>توئیتر </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+TOLOnews" target="_blank" title="گوگل پلاس"> <i class="fa fa-google-plus"></i> گوگل پلاس </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="یوتیوب"> <i class="fa fa-youtube-play"></i> یوتیوب </a>
                        </li>
                        <li class="last">
                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="اینستاگرام"> <i class="fa fa-instagram"></i> اینستاگرام </a>
                        </li>
                    </ul>
                </div>
                <!--END SOCIAL SHARE-->
            </div>
            <div class="row marginRow">
                <div class="inner-container">
                    <div class="one-col full-width-col-tablets">
                        <!--TOLONEWS POLL-->
                        <div class="twoo-column-tablets hidden-onmobile">
                            <h3 class="line red uppercase-txt line-red">نظرسنجی طلوع نیوز</h3>
                            <div class="grey-box box-vote bar-bottom">
                                <div class="title-category">آیا توافق میان دو نامزد ریاست جمهوری حل مسئله انتخابات برای اعلام نتیجه نهایی؟</div>
                                <form accept-charset="UTF-8" method="post" action="/">
                                    <ul>
                                        <li>
                                            <div class="question">
                                                <input id="choice1" type="radio" name="vote" value="Yes" />
                                                <label for="choice1">بله </label>
                                            </div>
                                            <div class="result">
                                                <span>بله </span><span class="red">(90%)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="question">
                                                <input id="choice2" type="radio" name="vote" value="No" />
                                                <label for="choice2">خیر </label>
                                            </div>
                                            <div class="result">
                                                <span>خیر</span><span class="red">(90%)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="question">
                                                <input id="choice3" type="radio" name="vote" value="No" />
                                                <label for="choice3">نمی دانم </label>
                                            </div>
                                            <div class="result">
                                                <span>نمی دانم </span><span class="red">(90%)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <input type="submit" class="link-square red button" value="رأی دهید" /> <a href="#" title="نتیجه آرا" class="link-square view-results">نتیجه آرا</a>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <!--END TOLONEWS POLL-->
                        <!-- TOLONEWS SCHEDULE-->
                        <div class="box-tolonews-program bar-bottom hidden-onmobile twoo-column-tablets last">
                            <h3 class="line green uppercase-txt line-green">جدول طلوع نیوز</h3>
                            <ul class="green-light">
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>آخرین بولتن خبری پنج دقیقه از طلوع نیوز</p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>آخرین بولتن خبری پنج دقیقه از طلوع نیوز</p>
                                </li>
                                <li class="schedule-link"> <a class="tolo-link green" rel="nofollow">برنامه های امروز ما</a> </li>
                            </ul>
                        </div>
                        <!--END TOLONEWS SCHEDULE-->
                    </div>
                    <!-- PHOTOS-->
                    <div class="two-col full-width-col-tablets last-col hidden-onmobile box-photo-right">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">عکس روز</h2>
                            <a title="تماشای همه عکس ها" href="#" class="view-all-post grey-light5">تماشای همه عکس ها</a>
                        </div>
                        <div id="panel">
                            <img src="images/image_01_large.jpg" alt="" width="650" height="434" class="container-image" id="largeImage" />
                            <div id="description" class="bar-bottom title-description">زمستان در افغانستان</div>
                        </div>
                        <div class="thumbs-photo">
                            <div class="thumbs-medium-column"> <img src="images/image_01_thumb.jpg" alt="مزار شریف" width="206" height="137" /> <span class="title-thumb">مزار شریف</span> </div>
                            <div class="thumbs-medium-column"> <img src="images/image_02_thumb.jpg" alt="شمال افغانستان" width="206" height="138" /> <span class="title-thumb">شمال افغانستان</span> </div>
                            <div class="thumbs-medium-column last"> <img src="images/image_03_thumb.jpg" alt="زمستان در افغانستان" width="206" height="138" /> <span class="title-thumb">زمستان در افغانستان</span> </div>
                        </div>
                    </div>
                    <!-- END PHOTOS-->
                </div>
            </div>
            <div class="row hidden-onmobile">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90" /> </a>
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <!-- OPINIONS-->
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">نظریات</h2>
                            <a title="تماشای همه نظریات" href="#" class="view-all-post grey-light5">تماشای همه نظریات</a>
                        </div>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">قتل فرخنده است: یک تراژدی ملی </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">کودکان در محل کار: میلیون ها نفر از کودکان در سراسر جهان باید برای غذا و سرپناه کار</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غنی و عبدالله نگاهی واشنگتن </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - 03:15 </span>
                                </div>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="بارگیری بیشتر">بارگیری بیشتر</a> </div>
                        <!-- END OPINIONS-->
                    </div>
                    <div class="one-col last-col noTopMargin-col">
                        <!-- TWITTER TOLONEWS-->
                        <div class="box box-tolo-twitter hidden-onmobile  hidden-ontablets">
                            <div class="container-top-post no-margin line line-blue clearfix">
                                <h3 class="titleBlock blue1 uppercase-txt">توئیتر طلوع نیوز</h3>
                                <a title="Tolonews" target="_blank" href="https://twitter.com/TOLOnews" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#43adef"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="light"
                                    data-border-color="#e9e9e9"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a data-show-count="false"  href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="blue1" target="back"> <i class="fa fa-twitter"></i> <span> را دنبال کنید @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!-- END TWITTER TOLONEWS-->
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer-af.php'; ?>
        </div>
    </body>
</html>
