<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tolonews</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!--<link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/tv-program.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    </head>
    <body class="is-exiting">
        <div class="overlay visible-onlymobile "></div>
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header.php'; ?>
        <div id="wrapper" class="wrapper row dark-tv-pages clearfix">
            <!--TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="visible-onlymobile top-post-tolonews post-single clearfix">
                <article>
                    <div class="video-content">
                        <iframe width="650" height="365" src="https://www.youtube.com/embed/2L_WTN4OOqc" allowfullscreen=""></iframe>
                    </div>
                    <div class="content-top-post-tolonews">
                        <div class="entry-headder">
                            <div class="tag-category"><a href="#" title="" class="type-category">Afghanistan</a><span class="post-date grey-light3">
                                <span class="uppercase-txt">May </span> 18, 2015 </span>
                            </div>
                            <h2 class="white title-top-post-tolonews">
                                Four Killed, Over 60 Injured in Zabul Suicide Bombing
                            </h2>
                        </div>
                        <div class="entry-blog-meta">
                            <ul class="share-buttons social-media">
                                <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                <li class="btn-social">
                                    <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="intro part-left full-width-col-mobile">
                            <p>At least four civilians were killed and more than 60 people - including children and women - were injured in a suicide attack in southern Zabul province on Monday, local officials said.</p>
                        </div>
                        <div class="entry-summary clearfix">
                            <p> he attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                <br />
                                he said.
                                Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                            </p>
                            <h3>Lorem ipsum big title</h3>
                            <p>
                                Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                <br />
                                Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                            </p>
                        </div>
                        <div class="entry-blog-meta">
                            <div class="container-top-post line line-grey hidden-onmobile">
                                <h2 class="title-category uppercase-txt">Share this post</h2>
                            </div>
                            <ul class="share-buttons social-media">
                                <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                <li class="list-icons">
                                                                        <ul class="first list-social">
                                                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                                                        </ul>
                                                                        <ul class="list-social second">
                                                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                                                        </ul>
                                                                    </li>
                                <li class="btn-social">
                                    <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
            <!--END TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <div class="top-post-tolonews post-single tv-program hidden-onmobile clearfix">
                            <article>
                                <div class="entry-headder">
                                    <div class="tag-category"><a href="#" title="" class="type-category">Afghanistan</a><span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                    <h2 class="white title-top-post-tolonews">
                                        Four Killed, Over 60 Injured in Zabul Suicide Bombing
                                    </h2>
                                </div>
                                <div class="entry-blog-meta">
                                    <ul class="share-buttons social-media">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                        <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                        <li class="btn-social">
                                            <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="video-content">
                                    <iframe width="650" height="365" src="https://www.youtube.com/embed/2L_WTN4OOqc" allowfullscreen=""></iframe>
                                </div>
                                <div class="entry-summary clearfix">
                                    <p> he attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                        <br />
                                        he said.
                                        Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                                    </p>
                                    <h3>Lorem ipsum big title</h3>
                                    <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                        <br />
                                        Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                                    </p>
                                </div>
                                <div class="entry-blog-meta" >
                                    <div class="container-top-post white line line-grey hidden-onmobile">
                                        <h2 class="title-category uppercase-txt">Share this post</h2>
                                    </div>
                                    <ul class="share-buttons social-media no-border">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                        <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                        <li class="btn-social">
                                            <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <!--RELATED STORIES-->
                        <div class="row-video clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category white uppercase-txt">Related stories</h2>
                            </div>
                            <ul class="three-col single-post">
                                <li class="post-tolonews one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="post-tolonews one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="post-tolonews last-col one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="post-tolonews one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="post-tolonews one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="post-tolonews last-col one-col">
                                    <a href="#" title="">
                                        <div class="container-image video-content">
                                            <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                            <div class="play"><i class="fa fa-play"></i> </div>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                National Football Team Heads Abroad to World Cup Qualifier
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- DISQUS COMMENTS-->
                        <div class="comment-post" id="scrol_to">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category white uppercase-txt">Comment this post</h2>
                            </div>
                            <div id="disqus_thread"></div>
                            <script>
                                /**
                                * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                */
                                /*
                                var disqus_config = function () {
                                this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                };
                                */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');

                                s.src = '//tolonewsdevargos.disqus.com/embed.js';

                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                        </div>
                        <!-- END DISQUS COMMENTS-->
                    </div>
                    <div class="fullWidth-pub marginRow hidden-onmobile show-onlytablets hidden centered">
                        <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90"> </a>
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col sidebarRight with-margin">
                        <!--SOCIAL SHARE-->
                        <div class="social-sidebar twoo-column-tablets hidden-onmobile clearfix">
                            <h3 class="line line-grey3 white uppercase-txt">Follow us </h3>
                            <div class="social-content post-tolonews">
                                <div class="box-social">
                                    <h3 class="title-article white">Join us on socials networks</h3>
                                    <ul class="social-media">
                                        <li>
                                            <a href="https://www.facebook.com/TOLOnews" target="_blank" title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://www.twitter.com/TOLOnews" target="_blank" title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/+TOLOnews" target="_blank" title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/user/TOLOnewsLive"  target="_blank" title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                    <div class="col-newsletter full-width-col-mobile">
                                        <h3 class="title-article white">Newsletter</h3>
                                        <p>Sign up to receive the bestof Tolo News daily</p>
                                        <form action="#" method="post">
                                            <label class="hide" for="email">Your email address </label>
                                            <input type="email" placeholder="Your email address" name="email" id="email">
                                            <input type="submit" value="ok">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--SOCIAL SHARE-->
                        <!--PUB-->
                        <div class="box pub equal-top-pub twoo-column-tablets last">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                        <div class="box-fb-timeline hidden-ontablets hidden-onmobile equal-top-pub">
                            <div class="container-top-post no-margin line line-grey">
                                <h3 class="titleBlock white uppercase-txt">Find us on facebook</h3>
                                <a  target="_blank" href="https://www.facebook.com/tolonews"  title="Tolonews" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="fb-page" data-href="https://www.facebook.com/tolonews" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/tolonews"><a href="https://www.facebook.com/tolonews">TOLOnews</a></blockquote>
                                </div>
                            </div>
                        </div>
                        <!--PUB-->
                        <div class="box equal-top-pub box-tolo-twitter hidden-onmobile  hidden-ontablets last">
                            <div class="container-top-post no-margin line line-grey">
                                <h3 class="titleBlock white uppercase-txt">Twitter feed</h3>
                                <a title="Tolonews" href="https://twitter.com/TOLOnews" target="_blank" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#cfcfcf"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="dark"
                                    data-border-color="#fff"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a  href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="white" target="back"> <i class="fa fa-twitter"></i> <span>Follow @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!--END PUB-->
                    </div>
                </div>
            </div>
            <div class="row hidden-onmobile hidden-ontablets">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90" /> </a>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer.php'; ?>
        </div>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
