var tolonews = tolonews || {};

(function ($){

    tolonews.headerSticky = function(context, settings) { "use strict";
        var $header = jQuery('header', context),
            $body = jQuery('body');

        $header.once('header').each(function() {
            var heightheader = $header.height();

            if(window.matchMedia("(min-device-width: 769px)").matches)
                heightheader = heightheader - 64;
            $header.height(heightheader);

            var checkHeaderHeight = true;

            jQuery(window).bind('resize', function(){
                if(checkHeaderHeight){
                    checkHeaderHeight = false;

                    if($body.hasClass('open-menu')){
                        closemenu();
                    }

                    setTimeout(function(){
                        var $navStick = jQuery('.navigation-sticky'),
                            $dropdownMenu = jQuery('.dropdown-menu'),
                            $latestNews = jQuery('.news-bar.latest');

                        if($navStick.hasClass('is-fixed')){
                            $navStick.removeClass('is-fixed');
                            $dropdownMenu.removeClass('is-fixed');
                            $latestNews.removeClass('is-fixed');
                            if(jQuery(window).scrollTop() >= $navStick.offset().top){

                                $navStick.addClass('is-fixed');
                                $dropdownMenu.addClass('is-fixed');
                                $latestNews.addClass('is-fixed');
                            }
                        }

                        setTimeout(function(){
                            $header.height('auto');

                            heightheader = $header.height();

                            if(window.matchMedia("(min-device-width: 769px)").matches)
                                heightheader = heightheader - 64;
                            $header.height(heightheader);

                            checkHeaderHeight = true;
                        },0);
                    },200);
                }
            });
        });

        var $navigationSticky = jQuery('.navigation-sticky', context);

        $navigationSticky.once('nav-sticky').each(function(){

            var windowWidth = jQuery(window).width();

            if (windowWidth > 767) {
                if(!$body.hasClass('desktopStiky')){
                    $body.addClass('desktopStiky').removeClass('mobileStiky');
                    scrollFuncForDesktop($navigationSticky, windowWidth);
                }
            }
            if ((windowWidth >= 320) && (windowWidth <= 767)) {
                if(!$body.hasClass('mobileStiky')){
                    $body.addClass('mobileStiky');
                    scrollFuncForMobile($navigationSticky);
                }

                jQuery(".wrapper").css({
                    "padding-top": '155px'
                });
            }

            var checkPaddingForContent = true;

            jQuery(window).bind('resize', function(){
                if(checkPaddingForContent){
                    checkPaddingForContent = false;
                    setTimeout(function(){
                        var height_sticky = $navigationSticky.height();

                        if (jQuery(window).width() > 767) {
                            if($navigationSticky.hasClass('is-fixed')){
                                $navigationSticky.removeClass('is-fixed');

                                setTimeout(function(){
                                    height_sticky = $navigationSticky.height();
                                    var group_top = jQuery('.group-top ').height(),
                                        news_bar = jQuery('.news-bar ').height();

                                    jQuery(".wrapper").css({
                                        "padding-top": group_top + height_sticky + news_bar + 30
                                    });

                                    if(!$body.hasClass('desktopStiky')){
                                        $body.addClass('desktopStiky').removeClass('mobileStiky');
                                        scrollFuncForDesktop($navigationSticky, windowWidth);
                                    }

                                    $navigationSticky.addClass('is-fixed');
                                }, 0);
                            }else{
                                height_sticky = $navigationSticky.height();
                                var group_top = jQuery('.group-top ').height(),
                                    news_bar = jQuery('.news-bar ').height();

                                jQuery(".wrapper").css({
                                    "padding-top": group_top + height_sticky + news_bar + 30
                                });

                                if(!$body.hasClass('desktopStiky')){
                                    $body.addClass('desktopStiky').removeClass('mobileStiky');
                                    scrollFuncForDesktop($navigationSticky, windowWidth);
                                }
                            }
                        }else{
                            jQuery(".wrapper").css({
                                "padding-top": '155px'
                            });

                            if(!$body.hasClass('mobileStiky')){
                                $body.addClass('mobileStiky').removeClass('desktopStiky');
                                scrollFuncForMobile($navigationSticky);
                            }
                        }

                        checkPaddingForContent = true;
                    },200);
                }
            });
        });

        var checkScrollSpeed = (function(settings){
            settings = settings || {};

            var lastPos, newPos, timer, delta,
                delay = settings.delay || 50; // in "ms" (higher means lower fidelity )

            function clear() {
                lastPos = null;
                delta = 0;
            }

            clear();

            return function(){
                newPos = window.scrollY;
                if ( lastPos != null ){ // && newPos < maxScroll
                    delta = newPos -  lastPos;
                }
                lastPos = newPos;
                clearTimeout(timer);
                timer = setTimeout(clear, delay);
                return delta;
            };
        })();

        function scrollFuncForMobile($navStick){
            jQuery(window).unbind('scroll.desktopHeaderScroll');

            var lastScrollTop      = 0,
                height_head        = jQuery('header').height() + 30,
                news_bar           = jQuery('.news-bar ').height(),
                headerHeightMobile = height_head + news_bar,
                $dropdown          = jQuery('.dropdown-menu'),
                thershold          = -5;

            jQuery(window).bind('scroll.mobileHeaderScroll', function(event){
                var st = jQuery(this).scrollTop(),
                    testSpeed = checkScrollSpeed();

                if (st > lastScrollTop){
                    // downscroll code
                    if($navStick.hasClass('is-fixed') && st > headerHeightMobile && thershold <= testSpeed) {
                        $navStick.removeClass('is-fixed');
                        $dropdown.removeClass('is-fixed');
                        $navStick.removeClass('fixed2');
                        $navStick.removeClass('fixed-revert');
                    }
                } else {
                    // upscroll code
                    if(!$navStick.hasClass('is-fixed') && thershold >= testSpeed) {
                        $navStick.addClass('is-fixed');
                        $dropdown.addClass('is-fixed');
                        $navStick.removeClass('fixed2');
                        $navStick.removeClass('fixed-revert');
                    }
                }

                lastScrollTop = st;
            });
        }

        function scrollFuncForDesktop($navigationSticky, windowWidth){
            jQuery(window).unbind('scroll.mobileHeaderScroll');

            var dropdownMenu = jQuery('.dropdown-menu'),
                latestNews = jQuery('.news-bar.latest'),
                height_sticky = $navigationSticky.height(),
                group_top = jQuery('.group-top ').height(),
                news_bar = jQuery('.news-bar ').height(),
                headerTop = $navigationSticky.offset().top;

            jQuery(".wrapper").css({
                "padding-top": group_top + height_sticky + news_bar + 30
            });

            var topLimit = (windowWidth > 1024) ? headerTop+53 : headerTop;

            if (jQuery(window).scrollTop() >= topLimit) {
                $navigationSticky.addClass('is-fixed');
                dropdownMenu.addClass('is-fixed');
                latestNews.addClass('is-fixed');
            }

            jQuery(window).bind('scroll.desktopHeaderScroll', function(event) {
                var y = jQuery(this).scrollTop();

                if (y >= topLimit) {
                    $navigationSticky.addClass('is-fixed');
                    dropdownMenu.addClass('is-fixed');
                    latestNews.addClass('is-fixed');
                } else {
                    $navigationSticky.removeClass('is-fixed');
                    dropdownMenu.removeClass('is-fixed');
                    latestNews.removeClass('is-fixed');
                }
            });
        }

        function closemenu() {
            var $body = jQuery('body'),
                $containerHeader = jQuery('.container-header');

            $body.removeClass('open-menu');
            $body.find('.mobile-overlay').remove();
            $containerHeader.removeClass('open').height('auto');
        }
    };

    tolonews.tolonewsSchedule = function(context, settings) { "use strict";
        //Schedule
        var $scheduleLink = jQuery('.box-tolonews-program .schedule-link', context);

        if(!$scheduleLink.length){ return false; }

        $scheduleLink.click(function(e) {
            var $boxProgramLi = jQuery('.box-tolonews-program li');

            $boxProgramLi.addClass('active');
            $scheduleLink.hide();

            e.preventDefault();
        });

    };

    tolonews.callJsVideoPlay = function(context, settings) { "use strict";
        // call js video PLAY
        var $containerVideo = jQuery('.container-video-cover', context);

        $containerVideo.once('callJsVideoPlay').each(function () {
            var $containerVideoPlay = $containerVideo.find('.play');

            $containerVideoPlay.click(function(event) {
                event.preventDefault();

                var $this = jQuery(this),
                    $thisConteiner = $this.closest('.container-video-cover');

                $thisConteiner.find('.video-hidden').toggleClass('hidden');
                $thisConteiner.find('.block-video-play').toggleClass('hide-cover');
            });
        });
    };

    tolonews.formSearchHeader = function(context, settings) { "use strict";
        // Search FORM
        var $formSearchHeader = jQuery('.form-search-header', context);

        if(!$formSearchHeader.length){ return false; }

        $formSearchHeader.on("click", function(e) {
            var $this = jQuery(this);

            if(!$this.hasClass('focus-input-visible')){
                jQuery(this).addClass('focus-input-visible');
            }
            e.stopPropagation();
        });

        jQuery(document).on("click", function(e) {
            if (jQuery(e.target).is(".form-search-header") === false) {
                if($formSearchHeader.hasClass('focus-input-visible')){
                    $formSearchHeader.removeClass("focus-input-visible");
                }
            }
        });
    };

    tolonews.socialBlock = function(context, settings) { "use strict";
        // Social block
        var $btnSocial = jQuery('.btn-social', context);

        if(!$btnSocial.length){ return false; }

        $btnSocial.once('social_open').on('click', function(event) {
            var $this = jQuery(this),
                social_container = $this.prev().children(".list-social.second"),
                windowWidth = jQuery(window).width();

            event.preventDefault();

            if (jQuery(social_container).hasClass('open')) {
                jQuery(social_container).removeClass('open');
            } else {
                jQuery(social_container).removeClass('open');
                jQuery(social_container).addClass('open');
            }

            if ((windowWidth >= 320) && (windowWidth <= 767)) {
                $this.parent().toggleClass('open');
                jQuery(".overlay").toggleClass('active');
             }
        });

        /*if ((windowWidth >= 320) && (windowWidth <= 767)) {
            $btnSocial.on('click', function(event) {
                event.preventDefault();
                jQuery(this).parent().toggleClass('open');
                jQuery(".overlay").toggleClass('active');
            });
        }*/

    };

    tolonews.scrollToSelf = function(context, settings) { "use strict";

        var $scrollTo = jQuery('a[href^="#scrol_to"]', context);

        if(!$scrollTo.length){ return false; }

        var height_head = jQuery('header').height() + 30;

        $scrollTo.click(function() {
            var the_id = jQuery(this).attr("href");
            jQuery('html, body').animate({
                scrollTop: jQuery(the_id).offset().top - height_head
            }, 'slow');
            return false;
        });
    };

    tolonews.mobileMenu = function(context, settings) { "use strict";
        // MENU MOBILE
        var $navbarToggle = jQuery('.navbar-toggle', context);

        $navbarToggle.on('click', function(event) {
            event.preventDefault();
            var heightbody = jQuery('#wrapper').outerHeight(),
                $containerHeader = jQuery(".container-header"),
                $body = jQuery('body');

            $containerHeader.height(heightbody);
            $containerHeader.addClass('open');
            jQuery(this).addClass('open');
            setTimeout(function() {
                if(!$body.hasClass('open-menu')){
                    $body.addClass('open-menu');

                    $body.prepend('<div class="mobile-overlay"></div>');
                }else{
                    closemenu();
                }
            }, 200);
            if (!jQuery(this).hasClass('open')) {
                setTimeout(function() {
                    $containerHeader.removeClass('open');
                }, 200);
            }
        });

        /* FONTION FERMETURE MENU MOBILE / TABLETTE */
        function closemenu() {
            var $body = jQuery('body'),
                $containerHeader = jQuery('.container-header');

            $body.removeClass('open-menu');
            $body.find('.mobile-overlay').remove();
            setTimeout(function(){
                $containerHeader.removeClass('open').height('auto');
            },200);
        }

        jQuery('#wrapper, .group-top, .bar-container').click(closemenu);

        jQuery(document).on('touchstart click', '.mobile-overlay', function() {
            closemenu();

            return false;
        });
    };

    tolonews.newsBarClose = function(context, settings) { "use strict";

        var $newsBarCloseBtn = jQuery('.news-bar .close', context);

        if(!$newsBarCloseBtn.length){ return false; }

        $newsBarCloseBtn.on('click', function(event) {
            event.preventDefault();

            var $newsBarBreaking = jQuery(".news-bar.breaking"),
                $newsBarlatest = jQuery(".news-bar.latest");

            $newsBarBreaking.attr('style', 'opacity: 0');
            $newsBarBreaking.removeClass("show");
            $newsBarlatest.attr('style', 'opacity: 1');
        });
    };

    tolonews.customUniForms = function(context, settings) { "use strict";
        //cal unfiorm js
        jQuery("select, input[type=radio], input[type=checkbox], input[type=file]").uniform();
    };

    tolonews.photosSlider = function(context, settings) { "use strict";
        //photos slider
        var $thumbsPhoto = jQuery('.thumbs-photo', context);

        if(!$thumbsPhoto.length){ return false; }

        $thumbsPhoto.delegate('.thumbs-medium-column > img', 'click', function() {
            var $this = jQuery(this),
                $bigImage = $this.closest('.thumbs-medium-column').find('.bigger img');

            jQuery('#largeImage').attr('src', $bigImage.attr('src'));
            jQuery('#description').html($this.find('+ .title-thumb').html());
        });
        $($thumbsPhoto.find('.thumbs-medium-column > img')[0]).click();
    };

    tolonews.tabulationsTolo = function(context, settings) { "use strict";
        //Global tabulation TOLONEWS
        var $tabsTolo = jQuery('ul.tabulations-tolo', context);

        if(!$tabsTolo.length){ return false; }

        $tabsTolo.once('tolo-tabs').each(function() {
            // For each set of tabs, we want to keep track of
            // which tab is active and it's associated content
            var $active, $content, $links = jQuery(this).find('a');

            // If the location.hash matches one of the links, use that as the active tab.
            // If no match is found, use the first link as the initial active tab.
            $active = jQuery($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
            $active.addClass('active');

            $content = jQuery($active[0].hash);

            // Hide the remaining content
            $links.not($active).each(function() {
                jQuery(this.hash).hide();
            });

            // Bind the click event handler
            jQuery(this).on('click', 'a', function(e) {
                // Make the old tab inactive.
                $active.removeClass('active');
                jQuery($active[0].hash).hide();

                // Update the variables with the new link and content
                $active = jQuery(this);
                $content = jQuery(this.hash);

                // Make the tab active.
                $active.addClass('active');
                $content.show();

                // Prevent the anchor's default click action
                e.preventDefault();
            });
        });
    };

    tolonews.latestNewsTransition = function(context, settings) { "use strict";
        // Latest news transitions
        var $news = jQuery('.news-bar.latest .news-items a', context);

        if(!$news.length){ return false; }

        $($news[0]).addClass('active');
        $news.once().each(function(){
            var $this = jQuery(this);

            $this.data('newsContent', $this.text());
        });

        function switchNews(news, nextNews){

            jQuery('.news-bar.latest .news-items a.active').fadeOut(300, function(){
                jQuery(this).removeClass('active');
                var nextNewsItem = jQuery(news.get(nextNews));
                nextNewsItem.html('').show();
                var typingSpeed = (2000 / nextNewsItem.data('newsContent').length > 30) ? 30 : (2000 / nextNewsItem.data('newsContent').length);
                var i = 0;
                var typing = setInterval(function(){
                    if(i >= nextNewsItem.data('newsContent').length){
                        clearInterval(typing);
                        return false;
                    }
                    nextNewsItem.html(nextNewsItem.html()+nextNewsItem.data('newsContent')[i]);
                    i++;
                }, typingSpeed);
                nextNewsItem.addClass('active');
            });
        }
        var currentNews = 0;
        setInterval(function(){
            var nextNews = (currentNews + 1 >= $news.length ) ? 0 : currentNews + 1;
            switchNews($news, nextNews);
            currentNews = nextNews;
        }, 6500);

        switchNews($news, 0);
    };

    tolonews.pageTransition = function(context, settings) { "use strict";
        // Page Transition
        var $isExiting = jQuery('.is-exiting', context);

        if(!$isExiting.length){ return false; }

        var $body = jQuery('body'),
            links = $body.find('a:not([target="_blank"]):not([href^=#]):not([rel="nofollow"])'); // Find all links except _blank and #

        links.each(function(key, index) {
            var $this = jQuery(index),
                url = $this.attr('href');

            $this.on('click', function(e) {
                e.preventDefault(); // Stop event
                $body.addClass('is-exiting');
                setTimeout(function() { // Timeout for animation
                    window.location = url; // Window.location allows us to keep the return button
                }, 200);
            });
        });

        $body.removeClass('is-exiting'); // Removing class for enter
    };

    tolonews.pollActions = function(context, settings) {
        // Create poll action links by code to avoid diffs with moсkups.

        var vote = $('.view-poll [id*="edit-vote"]');
        var back = $('.view-poll [id*="edit-back"]');

        if (vote.length) {
            var view = $('.view-poll [id*="edit-result"]');

            var view_link = $('<a/>')
                .attr('href', '#')
                .attr('title', view.val())
                .addClass('link-square view-results')
                .text(view.val())
                .click(function (e) {
                    e.preventDefault();
                    view.mousedown();
                });
            vote.once('fake-view-added').after(view_link);
            // We have space between elements on mockup html.
            view_link.before('&nbsp;');
        }

        if (back.length != 0) {
            $('.view-poll .poll dl').once('fake-back-added').after(back);
            back.wrap('<ul></ul>').wrap('<li></li>');
        }
    };

})(jQuery);

Drupal.behaviors.headerSticky = {
    attach: function(context, settings){
        tolonews.headerSticky(context, settings);
    }
};

Drupal.behaviors.tolonewsSchedule = {
    attach: function(context, settings){
        tolonews.tolonewsSchedule(context, settings);
    }
};

Drupal.behaviors.callJsVideoPlay = {
    attach: function(context, settings){
        tolonews.callJsVideoPlay(context, settings);
    }
};

Drupal.behaviors.formSearchHeader = {
    attach: function(context, settings){
        tolonews.formSearchHeader(context, settings);
    }
};

Drupal.behaviors.socialBlock = {
    attach: function(context, settings){
        tolonews.socialBlock(context, settings);
    }
};

Drupal.behaviors.scrollToSelf = {
    attach: function(context, settings){
        tolonews.scrollToSelf(context, settings);
    }
};

Drupal.behaviors.mobileMenu = {
    attach: function(context, settings){
        tolonews.mobileMenu(context, settings);
    }
};

Drupal.behaviors.newsBarClose = {
    attach: function(context, settings){
        tolonews.newsBarClose(context, settings);
    }
};

Drupal.behaviors.customUniForms = {
    attach: function(context, settings){
        tolonews.customUniForms(context, settings);
    }
};

Drupal.behaviors.photosSlider = {
    attach: function(context, settings){
        tolonews.photosSlider(context, settings);
    }
};

Drupal.behaviors.tabulationsTolo = {
    attach: function(context, settings){
        tolonews.tabulationsTolo(context, settings);
    }
};

Drupal.behaviors.latestNewsTransition = {
    attach: function(context, settings){
        tolonews.latestNewsTransition(context, settings);
    }
};

Drupal.behaviors.pageTransition = {
    attach: function(context, settings){
        tolonews.pageTransition(context, settings);
    }
};

Drupal.behaviors.pollActions = {
    attach: function(context, settings){
        tolonews.pollActions(context, settings);
    }
};
