<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tolonews</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!--<link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <link rel="stylesheet" type="text/css" href="css/rtl-app.css">
    </head>
    <body>
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header-af.php'; ?>
        <div id="wrapper" class="wrapper clearfix">
            <div class="row">
                <div class="inner-container">
                    <h1 class="red category-title">افغانستان <span>آرشیو</span></h1>
                    <div class="two-col full-width-col-tablets full-width-col-mobile clearfix archive">
                        <!-- ARCHIVE ARTICLE-->
                        <h2 class="title-category container-top-post line line-grey uppercase-txt">دوشنبه, 21 آوریل 2015</h2>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">قتل فرخنده است: یک تراژدی ملی </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">کودکان در محل کار: میلیون ها نفر از کودکان در سراسر جهان باید برای غذا و سرپناه کار </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna meu leo…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غنی و عبدالله نگاهی واشنگتن </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                        </ul>
                        <h2 class="title-category  container-top-post line line-grey uppercase-txt">یکشنبه، آوریل 20 2015</h2>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">قتل فرخنده است: یک تراژدی ملی </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">کودکان در محل کار: میلیون ها نفر از کودکان در سراسر جهان باید برای غذا و سرپناه کار </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna meu leo…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غنی و عبدالله نگاهی واشنگتن </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                        </ul>
                        <h2 class="container-top-post line line-grey title-category  uppercase-txt">یکشنبه، آوریل 20 2015</h2>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">قتل فرخنده است: یک تراژدی ملی </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">کودکان در محل کار: میلیون ها نفر از کودکان در سراسر جهان باید برای غذا و سرپناه کار </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna meu leo…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غنی و عبدالله نگاهی واشنگتن </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                        </ul>
                        <!-- END OPINIONS-->
                        <!-- PAGINATION-->
                        <ul class="pager">
                            <li class="pager-last previous"><a href="#" title="Aller à la page 3"> <i class="fa fa-angle-left red"></i><span>&nbsp;&nbsp;قبلی </span></a></li>
                            <li class="pager-item"><a href="#" title="Aller à la page 1">1</a></li>
                            <li class="pager-item pager-current"> 2 </li>
                            <li class="pager-item"><a href="#" title="Aller à la page 3">3</a></li>
                            <li class="pager-item"><a href="#" title="Aller à la page 4">4</a></li>
                            <li class="pager-item"><a href="#" title="Aller à la page 5">5</a></li>
                            <li class="pager-item"><a href="#" title="Aller à la page 6">6</a></li>
                            <li class="pager-last next"><a href="#" title="Aller à la page suvante"><span> بعد&nbsp;&nbsp;</span><i class="fa fa-angle-right red"></i></a></li>
                        </ul>
                        <!-- END PAGINATION-->
                    </div>
                    <div class="one-col last-col noTopMargin-col full-width-col-tablets hidden-onmobile">
                        <!--MOST READ / MOST WATCHED TABULATION-->
                        <div class="list-item-widget twoo-column-tablets bar-bottom">
                            <ul class="tabulations-tolo container-top-post no-margin line-red red">
                                <li class="titleBlock uppercase-txt"> <a class="active" href="#most-read">بیشتر خوانده شده‌ها/ پر بیننده‌ترین‌ها</a> </li>
                                <li class="grey-light5 uppercase-txt"> <a href="#most-watched">پر بازدیدترین‌ها</a> </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-read">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-watched">
                                <li>
                                    <a href="#"> <span class="number grey-light3">6</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">7</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">8</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">9</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">10</span> <span class="content">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END MOST READ / MOST WATCHED TABULATION-->
                        <!--PUB-->
                        <div class="box grey-box equal-top-pub twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250"> </a>
                        </div>
                        <!--END PUB-->
                        <!--SOCIAL SIDEBAR-->
                        <div class="social-sidebar equal-top-margin  hidden-onmobile  hidden-ontablets clearfix">
                            <h3 class="line line-grey3 grey-dark5 uppercase-txt">ما را دنبال کنید </h3>
                            <div class="social-content grey-dark5 post-tolonews">
                                <h3 class="title-article white">ما در شبکه های اجتماعی عضویت</h3>
                                <ul class="social-media">
                                    <li>
                                        <a href="https://www.facebook.com/TOLOnews" target="_blank" title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="http://www.twitter.com/TOLOnews" target="_blank" title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/+TOLOnews" target="_blank" title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                                    </li>
                                </ul>
                                <div class="col-newsletter full-width-col-mobile">
                                    <h3 class="title-article white">خبرنامه</h3>
                                    <p>ثبت نام برای دریافت بهترین طلوع نیوز روزانه</p>
                                    <form action="#" method="post">
                                        <label class="hide" for="email">آدرس ایمیل شما </label>
                                        <input type="email" placeholder="آدرس ایمیل شما" name="email" id="email">
                                        <input type="submit" value="موافقم">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--END SOCIAL SIDEBAR-->
                        <!-- TWITTER TOLONEWS-->
                        <div class="box box-tolo-twitter equal-top-margin hidden-onmobile  hidden-ontablets">
                            <div class="container-top-post no-margin line line-blue">
                                <h3 class="titleBlock blue1 uppercase-txt">توئیتر طلوع نیوز</h3>
                                <a href="https://twitter.com/TOLOnews" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#43adef"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="light"
                                    data-border-color="#e9e9e9"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a data-show-count="false"  href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="blue1" target="back"> <i class="fa fa-twitter"></i> <span>را دنبال کنید @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!-- END TWITTER TOLONEWS-->
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer-af.php'; ?>
        </div>
    </body>
</html>