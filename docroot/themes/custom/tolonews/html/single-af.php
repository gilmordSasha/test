<!DOCTYPE html>
<html lang="en">
    <head>
        <title>طلوع نیوز</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!-- <link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/home.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <link rel="stylesheet" href="css/rtl-app.css">
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    </head>
    <!--PAGE SINGLE-->
    <body class="page-single is-exiting">
        <div class="overlay visible-onlymobile "></div>
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header-af.php'; ?>
        <div id="wrapper" class="wrapper clearfix">
            <!--TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="visible-onlymobile top-post-tolonews post-single clearfix">
                <article>
                    <div class="full-width-col-mobile">
                        <a href="#" title=""> <img src="images/image-actu.jpg" alt="" width="440" height="295" /> </a>
                    </div>
                    <div class="content-top-post-tolonews">
                        <div class="entry-headder">
                            <div class="tag-category"><a href="#" title="" class="type-category">افغانستان</a><span class="post-date grey-light3">
                                <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                            </div>
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                چهار نفر کشته، بیش از 60 زخمی در زابل انتحاری
                            </h2>
                        </div>
                        <div class="entry-blog-meta">
                            <ul class="share-buttons social-media text-dark">
                                <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                    <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                <li class="btn-social">
                                    <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="intro part-left full-width-col-mobile">
                            <p>مقامات محلی گفتند، در یک حمله انتحاری در ولایت جنوبی زابل در روز دوشنبه مجروح شدند - حداقل چهار غیرنظامی کشته و بیش از 60 نفر - از جمله کودکان و زنان است.</p>
                        </div>
                        <div class="entry-summary clearfix">
                            <p>معاون رئیس پلیس غلام جیلانی فراهی گفت، او حمله در قلات در زمان مرکز استان پس از یک بمب گذار انتحاری مواد منفجره خود را در نزدیکی ساختمان شورای ولایتی را منفجر کردند. «بیشتر قربانیان غیرنظامی هستند. زنان و کودکان در میان مردم مجروح »شد <br />
                                <br />
                                او گفت.
                                فراهی گفت که پلیس تحقیقات در مورد این حمله آغاز کرده اند. مقامات گفتند که هیچ یک از اعضای شورای ولایتی است در این حمله صدمه دیده است. طالبان مسئولیت این حمله را برعهده گرفته اند.
                            </p>
                            <h3>تعدادی از روستاییان از مالستان</h3>
                            <p>تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است <br />
                                <br />
                                تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است
                            </p>
                        </div>
                        <div class="entry-blog-meta" >
                            <div class="container-top-post line line-grey hidden-onmobile">
                                <h2 class="title-category uppercase-txt">این اعلان را به اشتراک گذارید</h2>
                            </div>
                            <ul class="share-buttons social-media text-dark hidden-onmobile">
                                <li class="comments"><a href="#scrol_to" title="Comment"> تبصره‌ی۲۳۸۶۴</a></li>
                                    <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="اظهار نظر"><i class="fa commenting-o very-small-picto"></i><span>اظهار نظر</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                <li class="btn-social">
                                    <a href="#" title="اجتماعی بیشتر" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>اجتماعی بیشتر</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
            <!--END TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <div class="top-post-tolonews post-single hidden-onmobile clearfix">
                            <article>
                                <div class="entry-headder">
                                    <div class="tag-category"><a href="#" title="" class="type-category">افغانستان</a><span class="post-date grey-light3">
                                        <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                    </div>
                                    <h2 class="grey-dark1 title-top-post-tolonews">
                                        چهار نفر کشته، بیش از 60 زخمی در زابل انتحاری
                                    </h2>
                                </div>

                                <div class="entry-blog-meta">
                                    <ul class="share-buttons social-media text-dark hidden-onmobile">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> تبصره‌ی۲۳۸۶۴</a></li>
                                        <li class="list-icons">
                                            <ul class="first list-social">
                                                <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                <li><a href="#scrol_to" title="اظهار نظر"><i class="fa commenting-o very-small-picto"></i><span>اظهار نظر</span></a></li>
                                            </ul>
                                            <ul class="list-social second">
                                                <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                            </ul>
                                        </li>
                                        <li class="btn-social">
                                            <a href="#" title="اجتماعی بیشتر" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>اجتماعی بیشتر</span></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="intro part-left full-width-col-mobile">
                                    <p>مقامات محلی گفتند، در یک حمله انتحاری در ولایت جنوبی زابل در روز دوشنبه مجروح شدند - حداقل چهار غیرنظامی کشته و بیش از 60 نفر - از جمله کودکان و زنان است.</p>
                                </div>
                                <div class="part-right full-width-col-mobile">
                                    <a href="#" title=""> <img src="images/image-actu.jpg" alt="" width="440" height="295" /> </a>
                                </div>
                                <div class="entry-summary clearfix">
                                    <p>معاون رئیس پلیس غلام جیلانی فراهی گفت، او حمله در قلات در زمان مرکز استان پس از یک بمب گذار انتحاری مواد منفجره خود را در نزدیکی ساختمان شورای ولایتی را منفجر کردند. «بیشتر قربانیان غیرنظامی هستند. زنان و کودکان در میان مردم مجروح »شد <br />
                                        <br />
                                        او گفت. فراهی گفت که پلیس تحقیقات در مورد این حمله آغاز کرده اند. مقامات گفتند که هیچ یک از اعضای شورای ولایتی است در این حمله صدمه دیده است. طالبان مسئولیت این حمله را برعهده گرفته اند.
                                    </p>
                                    <h3>تعدادی از روستاییان از مالستان</h3>
                                    <p>
                                        تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است <br />
                                        <br />
                                        تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است
                                    </p>
                                </div>
                                <div class="entry-blog-meta hidden-onmobile">
                                    <div class="container-top-post line line-grey">
                                        <h2 class="title-category uppercase-txt">این اعلان را به اشتراک گذارید</h2>
                                    </div>
                                    <ul class="share-buttons social-media text-dark no-border">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> تبصره‌ی۲۳۸۶۴</a></li>
                                        <li class="list-icons">
                                            <ul class="first list-social">
                                                <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                <li><a href="#scrol_to" title="اظهار نظر"><i class="fa commenting-o very-small-picto"></i><span>اظهار نظر</span></a></li>
                                            </ul>
                                            <ul class="list-social second">
                                                <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                            </ul>
                                        </li>
                                        <li class="btn-social">
                                            <a href="#" title="اجتماعی بیشتر" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>اجتماعی بیشتر</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <!--DEBUT RELATED STORIES-->
                        <div class="related-post clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">داستان های مرتبط</h2>
                                <a href="#" title="View all World news" class="view-all-post grey-light5 visible-onlymobile">مشاهده همه </a>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                        <!-- DISQUS COMMENTS-->
                        <div class="comment-post" id="scrol_to">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">نظر این پست</h2>
                            </div>
                            <div id="disqus_thread"></div>
                            <script>
                                /**
                                * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                */
                                /*
                                var disqus_config = function () {
                                this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                };
                                */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');

                                s.src = '//tolonewsdevargos.disqus.com/embed.js';

                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>لطفا JavaScript را فعال برای مشاهده <a href="https://disqus.com/?ref_noscript" rel="nofollow">نظر توسط Disqus.</a></noscript>
                        </div>
                        <!-- END DISQUS COMMENTS-->
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col sidebarRight with-margin">
                        <!--LATEST AFGHANISTAN NEWS-->
                        <div class="twoo-column-tablets list-item-widget clearfix bar-bottom  hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">آخرین اخبار افغانستان</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"><span class="content category">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END LATEST AFGHANISTAN NEWS-->
                        <!--PUB-->
                        <div class="box grey-box equal-top-pub twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                        <!--MOST POPULAR IN AFGHANISTAN-->
                        <div class="equal-top-pub right-col-tablets last-col list-item-widget clearfix bar-bottom twoo-column-tablets hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">محبوب ترین در افغانستان</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END MOST POPULAR IN AFGHANISTAN-->
                        <!--SOCIAL SHARE-->
                        <div class="social-sidebar equal-top-margin twoo-column-tablets hidden-onmobile clearfix">
                            <h3 class="line line-grey3 grey-dark5 uppercase-txt">ما را دنبال کنید</h3>
                            <div class="social-content post-tolonews">
                                <div class="box-social">
                                    <h3 class="title-article white">ما در شبکه های اجتماعی عضویت</h3>
                                    <ul class="social-media">
                                        <li>
                                            <a href="https://www.facebook.com/TOLOnews" target="_blank"  title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://www.twitter.com/TOLOnews" target="_blank"  title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/+TOLOnews" target="_blank"  title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank"  title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank"  title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                    <div class="col-newsletter full-width-col-mobile">
                                        <h3 class="title-article white">خبرنامه</h3>
                                        <p>ثبت نام برای دریافت بهترین طلوع نیوز روزانه</p>
                                        <form action="#" method="post">
                                            <label class="hide" for="email">آدرس ایمیل شما </label>
                                            <input type="email" placeholder="آدرس ایمیل شما" name="email" id="email">
                                            <input type="submit" value="موافقم">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--SOCIAL SHARE-->
                    </div>
                </div>
            </div>
            <div class="row hidden-onmobile">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90" /> </a>
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="post-category clearfix inline-three-column">
                        <div class="container-top-post line line-grey">
                            <h2 class="title-category uppercase-txt">همچنین در اخبار</h2>
                            <a href="#" title="View all World news" class="view-all-post grey-light5 visible-onlymobile">مشاهده همه </a>
                        </div>
                        <div class="column one-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">بازرگانی</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </article>
                        </div>
                        <div class="column one-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">انتخابات</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، شرکت های آمریکایی</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </article>
                        </div>
                        <div class="column one-col last-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img44.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">جهان</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">مردان مسلح ناشناس ربودن 19 کارمندان ماین پاکی در Pakti</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </article>
                        </div>
                        <!--show-onlytablets block-->
                        <div class="one-col box-tolonews-program bar-bottom column  show-onlytablets hidden last">
                            <h3 class="line green uppercase-txt line-green">جدول طلوع نیوز</h3>
                            <ul class="green-light">
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">On air</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">To follow </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>آخرین بولتن خبری پنج دقیقه از طلوع نیوز</p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li> <a href="#" class="tolo-link green">برنامه های امروز ما</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer-af.php'; ?>
        </div>
    </body>
</html>
