<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tolonews</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!-- <link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/home.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    </head>
    <!--PAGE SINGLE-->
    <body class="page-single is-exiting">
        <div class="overlay visible-onlymobile "></div>
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header.php'; ?>
        <div id="wrapper" class="wrapper clearfix">
            <!--TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="visible-onlymobile top-post-tolonews post-single clearfix">
                <article>
                    <div class="full-width-col-mobile">
                        <a href="#" title=""> <img src="images/image-actu.jpg" alt="" width="440" height="295" /> </a>
                    </div>
                    <div class="content-top-post-tolonews">
                        <div class="entry-headder">
                            <div class="tag-category"><a href="#" title="" class="type-category">Afghanistan</a><span class="post-date grey-light3">
                                <span style="color:darkgrey;">By Kathy Whitehead</span>
								<span class="uppercase-txt">May </span> <span>18, 2015 - Edited:</span>
								<span class="uppercase-txt">May </span> <span>18, 2015 20:33</span>
                            </div>
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                Four Killed, Over 60 Injured in Zabul Suicide Bombing
                            </h2>
                        </div>
                        <div class="entry-blog-meta">
                            <ul class="share-buttons social-media text-dark">
                                <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                               <li class="list-icons">
                                            <ul class="first list-social">
                                                <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                            </ul>
                                            <ul class="list-social second">
                                                <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                            </ul>
                                        </li>
                                <li class="btn-social">
                                    <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="intro part-left full-width-col-mobile">
                            <p>At least four civilians were killed and more than 60 people - including children and women - were injured in a suicide attack in southern Zabul province on Monday, local officials said.</p>
                        </div>
                        <div class="entry-summary clearfix">
                            <p> he attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                <br />
                                he said.
                                Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                            </p>
                            <h3>Lorem ipsum big title</h3>
                            <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                <br />
                                Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                            </p>
                        </div>
                        <div class="entry-blog-meta" >
                            <div class="container-top-post line line-grey hidden-onmobile">
                                <h2 class="title-category uppercase-txt">Share this post</h2>
                            </div>
                            <ul class="share-buttons social-media text-dark">
                                <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                    <li class="list-icons">
                                        <ul class="first list-social">
                                            <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                            <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                            <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                            <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                        </ul>
                                        <ul class="list-social second">
                                            <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                            <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                            <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                            <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                        </ul>
                                    </li>
                                <li class="btn-social">
                                    <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
            <!--END TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <div class="top-post-tolonews post-single hidden-onmobile clearfix">
                            <article>
                                <div class="entry-headder">
                                    <div class="tag-category"><a href="#" title="" class="type-category">Afghanistan</a><span class="post-date grey-light3">
                                <span style="color:darkgrey;">By Kathy Whitehead</span>
								<span class="uppercase-txt">May </span> <span>18, 2015 - Edited:</span>
								<span class="uppercase-txt">May </span> <span>18, 2015 20:33</span>
                            </div>
                                    <h2 class="grey-dark1 title-top-post-tolonews">
                                        Four Killed, Over 60 Injured in Zabul Suicide Bombing
                                    </h2>
                                </div>

                                <div class="entry-blog-meta">
                                    <ul class="share-buttons social-media text-dark">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                        <li class="list-icons">
                                            <ul class="first list-social">
                                                <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                            </ul>
                                            <ul class="list-social second">
                                                <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                            </ul>
                                        </li>
                                        <li class="btn-social">
                                            <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="intro part-left full-width-col-mobile">
                                    <p>At least four civilians were killed and more than 60 people - including children and women - were injured in a suicide attack in southern Zabul province on Monday, local officials said.</p>
                                </div>
                                <div class="part-right full-width-col-mobile">
                                    <a href="#" title=""> <img src="images/image-actu.jpg" alt="" width="440" height="295" /> </a>
                                </div>
                                <div class="entry-summary clearfix">
                                    <p> he attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                        <br />
                                        he said.
                                        Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                                    </p>
                                    <h3>Lorem ipsum big title</h3>
                                    <p>
                                        Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                        <br />
                                        Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                                    </p>
                                </div>
                                <div class="entry-blog-meta hidden-onmobile">
                                    <div class="container-top-post line line-grey">
                                        <h2 class="title-category uppercase-txt">Share this post</h2>
                                    </div>
                                    <ul class="share-buttons social-media text-dark no-border">
                                        <li class="comments"><a href="#scrol_to" title="Comment"> 254 Comments</a></li>
                                        <li class="list-icons">
                                            <ul class="first list-social">
                                                <li><span class='share-btn st_facebook_large' displayText='Facebook'></span></li>
                                                <li><span class='share-btn st_twitter_large' displayText='Tweet'></span></li>
                                                <li><span class='share-btn st_email_large' displayText='Email'></span></li>
                                                <li><a href="#scrol_to" title="Comment"><i class="fa commenting-o very-small-picto"></i><span>Comment</span></a></li>
                                            </ul>
                                            <ul class="list-social second">
                                                <li><span class='share-btn st_reddit_large' displayText='Reddit'></span></li>
                                                <li><span class='share-btn st_linkedin_large' displayText='LinkedIn'></span></li>
                                                <li><span class='share-btn st_tumblr_large' displayText='Tumblr'></span></li>
                                                <li><span class='share-btn st_googleplus_large' displayText='Google+'></span></li>
                                            </ul>
                                        </li>
                                        <li class="btn-social">
                                            <a href="#" title="More" class="no-margin"><i class="fa fa-plus very-small-picto"></i><span>More socials</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <!--DEBUT RELATED STORIES-->
                        <div class="related-post clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">Related stories</h2>
                                <a href="#" title="View all World news" class="view-all-post grey-light5 visible-onlymobile">View all </a>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
											<span style="color:dark grey;">By Kathy Whitehead</span>
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                        <!-- DISQUS COMMENTS-->
                        <div class="comment-post" id="scrol_to">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">Comment this post</h2>
                            </div>
                            <div id="disqus_thread"></div>
                            <script>
                                /**
                                * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                */
                                /*
                                var disqus_config = function () {
                                this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                };
                                */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');

                                s.src = '//tolonewsdevargos.disqus.com/embed.js';

                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                        </div>
                        <!-- END DISQUS COMMENTS-->
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col sidebarRight with-margin">
                        <!--LATEST AFGHANISTAN NEWS-->
                        <div class="twoo-column-tablets list-item-widget clearfix bar-bottom  hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">Latest Afghanistan news</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"><span class="content category">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">Police Seize Over 1,000kgs Of Opium, 900kgs Hashish
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">  $6 Million Dollars Spent So Far To Track 31 Hostages
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category"> Taliban And Daesh Declare Jihad Against Each Other
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"><span class="content category">   Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END LATEST AFGHANISTAN NEWS-->
                        <!--PUB-->
                        <div class="box grey-box equal-top-pub twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                        <!--MOST POPULAR IN AFGHANISTAN-->
                        <div class="equal-top-pub right-col-tablets last-col list-item-widget clearfix bar-bottom twoo-column-tablets hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">MOST POPULAR IN AFGHANISTAN</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">Police Seize Over 1,000kgs Of Opium, 900kgs Hashish
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  $6 Million Dollars Spent So Far To Track 31 Hostages
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> Taliban And Daesh Declare Jihad Against Each Other
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">   Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END MOST POPULAR IN AFGHANISTAN-->
                        <!--SOCIAL SHARE-->
                        <div class="social-sidebar equal-top-margin twoo-column-tablets hidden-onmobile clearfix">
                            <h3 class="line line-grey3 grey-dark5 uppercase-txt">Follow us </h3>
                            <div class="social-content post-tolonews">
                                <div class="box-social">
                                    <h3 class="title-article white">Join us on socials networks</h3>
                                    <ul class="social-media">
                                        <li>
                                            <a href="https://www.facebook.com/TOLOnews" target="_blank"  title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://www.twitter.com/TOLOnews" target="_blank"  title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/+TOLOnews" target="_blank"  title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank"  title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank"  title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                    <div class="col-newsletter full-width-col-mobile">
                                        <h3 class="title-article white">Newsletter</h3>
                                        <p>Sign up to receive the bestof Tolo News daily</p>
                                        <form action="#" method="post">
                                            <label class="hide" for="email">Your email address </label>
                                            <input type="email" placeholder="Your email address" name="email" id="email">
                                            <input type="submit" value="ok">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--SOCIAL SHARE-->
                    </div>
                </div>
            </div>
            <div class="row hidden-onmobile">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90" /> </a>
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="post-category clearfix inline-three-column">
                        <div class="container-top-post line line-grey">
                            <h2 class="title-category uppercase-txt">Also in the news</h2>
                            <a href="#" title="View all World news" class="view-all-post grey-light5 visible-onlymobile">View all </a>
                        </div>
                        <div class="column one-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">Business</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">West Imposes New Russia Sanctions, Putin Defiant</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">WDonec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </article>
                        </div>
                        <div class="column one-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">Election</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">Builders Protest Over Non-Payment By Turkish, US Companies</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">WDonec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </article>
                        </div>
                        <div class="column one-col last-col">
                            <article class="post-tolonews">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/img44.jpg" alt="" width="310" height="206"> </a>
                                    <div class="tag-category"><span class="type-category">World</span></div>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">Unknown Gunmen Abduct 19 Deminers In Pakti</a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">WDonec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada...</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </article>
                        </div>
                        <!--show-onlytablets block-->
                        <div class="one-col box-tolonews-program bar-bottom column  show-onlytablets hidden last">
                            <h3 class="line green uppercase-txt line-green">Tolonews Schedule</h3>
                            <ul class="green-light">
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">On air</span> <span> World Update</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">To follow </span> <span> TOLO News</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>The lastest five minute news bulletin from Tolo News</p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> World Update</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li> <a href="#" class="tolo-link green">View Today's schedule</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer.php'; ?>
        </div>
    </body>
</html>
