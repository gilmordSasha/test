<footer class="row grey-dark">
    <section class="inner-container footer-section">
        <div class="middle">
            <div class="media full-width-col-mobile one-col last-col">
                <a href="#" title=""><img src="images/logo-footer.png" alt="logo" width="170" height="auto" /></a>
                <ul class="social-media">
                    <li>
                        <a href="https://www.facebook.com/TOLOnews" target="_blank" title="فیس بوک"><i class="fa meduim-play fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="http://www.twitter.com/TOLOnews"  target="_blank" title="توئیتر"><i class="fa meduim-play fa-twitter"></i> </a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/+TOLOnews" target="_blank" title="گوگل پلاس"><i class="fa meduim-play fa-google-plus"></i></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="یوتیوب"><i class="fa meduim-play fa-youtube-play"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="اینستاگرام"><i class="fa small-picto fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav hidden-onmobile">
                <div class="inline-columnFooter">
                    <h4>طلوع نیوز</h4>
                    <ul>
                        <li><a href="#" title="خانه">خانه</a></li>
                        <li><a href="#" title="سیاست">سیاست</a></li>
                        <li><a href="#" title="جهان">جهان</a></li>
                        <li><a href="#" title="نظر شما">نظر شما</a></li>
                        <li><a href="#" title="رسانه ها">رسانه ها</a></li>
                        <li><a href="#" title="فرهنگ و هنر">فرهنگ و هنر</a></li>
                        <li><a href="#" title="ورزش">ورزش</a></li>
                        <li><a href="#" title="بازرگانی">بازرگانی</a></li>
                    </ul>
                </div>
                <div class="inline-columnFooter last">
                    <h4>انتخابات</h4>
                    <ul>
                        <li><a href="#" title="خانه">خانه</a></li>
                        <li><a href="#" title="خبر">خبر</a></li>
                        <li><a href="#" title="نامزدها">نامزدها</a></li>
                        <li><a href="#" title="بحث و تحلیل">بحث و تحلیل</a></li>
                        <li><a href="#" title="ولایتی">ولایتی</a></li>
                        <li><a href="#" title="ویدیوها">ویدیوها</a></li>
                        <li><a href="#" title="پاسخ به سئوالات">پاسخ به سئوالات</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-newsletter full-width-col-mobile">
                <h4>خبرنامه</h4>
                <p>برای دریافت بهترین اخبار روز از طلوع نیوز ثبت نام کنید </p>
                <form method="post" action="#">
                    <label for="email" class="hide">آدرس ایمیل شما  </label>
                    <input id="email" type="email"
                        name="email" placeholder="آدرس ایمیل شما ">
                    <input type="submit"
                        value="موافقم">
                </form>
                <ul class="post-numbers hidden-onmobile">
                    <li><span class="numbers">۳۰۵۷</span><span> مطلب</span>
                    </li>
                    <li><span class="numbers">۲۳۸۶۴</span><span> تبصره‌ی</span>
                    </li>
                    <li><span class="numbers">۹۵۷۴۰۰</span><span>دنبال کننده‌ی</span>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav-copyright clearfix">
            <li class="one-col full-width-col-mobile">©2014 طلوع نیوز - تمام حقوق طلوع نیوز محفوظ است
            </li>
            <li class="one-col full-width-col-mobile">
                <ul class="nav">
                    <li><a href="#" title="درباره‌ی ما">درباره‌ی ما</a></li>
                    <li><a href="#" title="شرایط استفاده">شرایط استفاده</a></li>
                    <li><a href="#" title="تبلیغات خود را به ما بسپارید">تبلیغات خود را به ما بسپارید</a></li>
                    <li><a href="#" title="تماس">تماس</a></li>
                </ul>
            </li>
            <li class="one-col last-col full-width-col-mobile">سایت های به Colorz</li>
        </ul>
    </section>
</footer>