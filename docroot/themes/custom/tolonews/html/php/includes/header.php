<header>
  <div class="group-top hidden-onmobile">
    <div class="inner-container">
      <img src="images/pub-top.jpg" alt="pub top" width="728" height="90" />
    </div>
  </div>
  <div class="navigation-sticky">
    <div class=" group-middle clearfix">
      <div class="inner-container">
        <div class="logo">
          <h1>
            <a title="Accueil" href="/">
              <img src="images/logo.png" alt="logo" width="91" height="91" />
            </a>
          </h1>
        </div>
        <button type="button" class="navbar-toggle">
          <span>MENU</span>
        </button>
        <div class="container-header">
          <div class="header-right clearfix">
            <div class="search">
              <form class="form-search-header" accept-charset="UTF-8" method="post" action="/">
                <label class="hide" for="edit-search">Rechercher </label>
                <input type="submit" class="form-submit" id="search-submit">
                <input id="edit-search" type="text" placeholder="Search on TOLOnews">
              </form>
            </div>
            <div class="top-navigation clearfix">
              <ul>
                <li><a href="archives.php">About</a>
                </li>
                <li><a href="#">Contact</a>
                </li>
                <li><a href="#">Newsletter</a>
                </li>
              </ul>
              <ul class="language">
                <li class="bold"><a href="#" class="ltr">English</a>
                </li>
                <li><a href="#" class="rtl">پشتو</a>
                </li>
                <li><a href="#" class="rtl">فارسی</a>
                </li>
              </ul>
            </div>
          </div>
          <nav>
            <ul class="nav navbar-nav clearfix">
              <li class="lvl1"><a href="#" title="Home">Home</a>
              </li>
              <li class="lvl1">
                <a href="#" class="dropdown-toggle" title="Afghanistan">Afghanistan</a>
                <div class="dropdown-menu">
                  <div class="inner-container">
                    <div class="one-col">
                      <h2 class="red">Afghanistan</h2>
                      <ul class="list-menu">
                        <li><a href="#" title="">All</a>
                        </li>
                        <li><a href="#" title="">PARLIAMENT</a>
                        </li>
                        <li><a href="#" title="">PROVINCIAL</a>
                        </li>
                      </ul>
                    </div>
                    <div class="two-col last-col">
                      <ul class="row full-width-col-mobile list-post ">
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img4.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img6.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img5.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img7.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="lvl1">
                <a href="#" title="Business">Business </a>
                <div class="dropdown-menu">
                  <div class="inner-container">
                    <div class="one-col">
                      <h2 class="red">Business</h2>
                      <ul class="list-menu">
                        <li><a href="#" title="All">All</a>
                        </li>
                        <li><a href="#" title="PARLIAMENT">PARLIAMENT</a>
                        </li>
                        <li><a href="#" title="PROVINCIAL">PROVINCIAL</a>
                        </li>
                      </ul>
                    </div>
                    <div class="two-col last-col">
                      <ul class="row full-width-col-mobile list-post ">
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img44.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img55.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img5.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img77.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="lvl1">
                <a href="#" class="dropdown-toggle" title="Election">Election </a>
                <div class="dropdown-menu">
                  <div class="inner-container">
                    <div class="one-col">
                      <h2 class="red">Election</h2>
                      <ul class="list-menu">
                        <li><a href="#" title="All">All</a>
                        </li>
                        <li><a href="#" title="PARLIAMENT">PARLIAMENT</a>
                        </li>
                        <li><a href="#" title="PROVINCIAL">PROVINCIAL</a>
                        </li>
                      </ul>
                    </div>
                    <div class="two-col last-col">
                      <ul class="row full-width-col-mobile list-post ">
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img4.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img6.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img5.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img7.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="lvl1">
                <a href="#" title="World">World </a>
                <div class="dropdown-menu">
                  <div class="inner-container">
                    <div class="one-col">
                      <h2 class="red">World</h2>
                      <ul class="list-menu">
                        <li><a href="#" title="All">All</a>
                        </li>
                        <li><a href="#" title="PARLIAMENT">PARLIAMENT</a>
                        </li>
                        <li><a href="#" title="PROVINCIAL">PROVINCIAL</a>
                        </li>
                      </ul>
                    </div>
                    <div class="two-col last-col">
                      <ul class="row full-width-col-mobile list-post ">
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img44.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img55.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img5.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img77.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="lvl1">
                <a href="#" class="dropdown-toggle" title="Opinion">Opinion </a>
                <div class="dropdown-menu">
                  <div class="inner-container">
                    <div class="one-col">
                      <h2 class="red">Opinion</h2>
                      <ul class="list-menu">
                        <li><a href="#" title="All">All</a>
                        </li>
                        <li><a href="#" title="PARLIAMENT">PARLIAMENT</a>
                        </li>
                        <li><a href="#" title="PROVINCIAL">PROVINCIAL</a>
                        </li>
                      </ul>
                    </div>
                    <div class="two-col last-col">
                      <ul class="row full-width-col-mobile list-post ">
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img4.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img6.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img5.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">
                                  <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                </a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                        <li class="one-col last-col">
                          <article class="post-tolonews small-column">
                            <div class="container-image">
                              <a href="#" title="">
                                <img src="images/img7.png" alt="" width="147" height="98" />
                              </a>
                            </div>
                            <div class="content-article">
                              <h4>
                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                              </h4>
                              <span class="post-date grey-light3">
                              <span class="uppercase-txt">May </span> 18, 2015
                              </span>
                            </div>
                          </article>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li class="lvl1">
                <a href="#" title="Video">Video </a>
              </li>
              <li class="lvl1">
                <a href="#" title="Arts &amp; Culture">Arts &amp; Culture </a>
              </li>
              <li class="lvl1">
                <a href="#" title="Sports">Sports </a>
              </li>
              <li>
                <a href="#" class="watch-link bold" title="WATCH">
                  WATCH
                  <span>LIVE</span>
                </a>
              </li>
            </ul>
          </nav>
          <!--  show in mobile    -->
          <div class="top-navigation clearfix show-in-mobile">
            <ul>
              <li><a href="#">About</a>
              </li>
              <li><a href="#">Contact</a>
              </li>
              <li><a href="#">Newsletter</a>
              </li>
            </ul>
            <ul class="language">
              <li class="bold"><a href="#" class="ltr">English</a>
              </li>
              <li><a href="#" class="rtl">پشتو</a>
              </li>
              <li><a href="#" class="rtl">فارسی</a>
              </li>
            </ul>
          </div>
        </div>
        <!--/.nav-collapse -->
      </div>
    </div>
    <div class="bar-container">
      <div class="news-bar breaking show">
        <div class="inner-container news">
          <div class="latest">Breaking news</div>
          <a href="#">EGYPT JUGEMENT: Egypt's former President Morsi jailed for 20 years</a> <a href="#" title="" class="close">x</a>
        </div>
      </div>
    </div>
  </div>
  <div class="news-bar latest">
    <div class="inner-container news">
      <div class="latest">Latest news</div>
      <div class="news-items">
        <a href="#" class="active">EGYPT JUGEMENT: Egypt's former President Morsi jailed for 20 years</a>
        <a href="#">EGYPT JUGEMENT 2: Egypt's former President Morsi jailed for 20 years</a>
        <a href="#">EGYPT JUGEMENT 3: Egypt's former President Morsi jailed for 20 years</a>
      </div>
    </div>
  </div>
</header>
