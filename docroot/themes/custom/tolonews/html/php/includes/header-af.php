<header>
    <div class="group-top hidden-onmobile">
        <div class="inner-container">
            <img src="images/pub-top.jpg" alt="pub top" width="728" height="90" />
        </div>
    </div>
    <div class="navigation-sticky">
        <div class=" group-middle clearfix">
  			<div class="inner-container">
            <div class="logo">
                <h1>
                    <a title="Accueil" href="/">
                        <img src="images/logo-af.jpg" alt="logo" width="91" height="91" />
                    </a>
            </h1>
            </div>
            <button type="button" class="navbar-toggle">
            <span>فهرست</span>
            </button>
            <div class="container-header">
                <div class="header-right clearfix">
                    <div class="search">
                        <form class="form-search-header" accept-charset="UTF-8" method="post" action="/">
                            <label class="hide" for="edit-search">جستجو </label>
                            <input type="submit" class="form-submit" id="search-submit">
                             <input id="edit-search" type="text" placeholder="در طلوع نیوز جست‌وجو کنید /جست‌وجو">
                        </form>
                    </div>
                    <div class="top-navigation clearfix">
                        <ul>
                            <li><a href="#">درباره‌</a></li>
                            <li><a href="#">تماس با ما</a></li>
                            <li><a href="#">خبرنامه</a></li>
                        </ul>
                        <ul class="language">
                            <li class="bold"><a href="index.php" class="ltr">English</a></li>
                            <li><a href="home-af.php" class="rtl">پشتو</a></li>
                            <li><a href="home-af.php" class="rtl">فارسی</a></li>
                        </ul>
                    </div>
                </div>
                <nav>
                    <ul class="nav navbar-nav clearfix">
                        <li class="lvl1"><a href="#" title="خانه">خانه</a></li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="افغانستان">افغانستان</a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">افغانستان</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="">همه</a></li>
                                            <li><a href="#" title="">مجلس</a></li>
                                            <li><a href="#" title="">ولایتی</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="بازرگانی">بازرگانی </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">بازرگانی</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="همه">همه</a></li>
                                            <li><a href="#" title="مجلس">مجلس</a></li>
                                            <li><a href="#" title="ولایتی">ولایتی</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img44.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img55.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img77.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="انتخابات">انتخابات </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">انتخابات</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="همه">همه</a></li>
                                            <li><a href="#" title="مجلس">مجلس</a></li>
                                            <li><a href="#" title="ولایتی">ولایتی</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="جهان">جهان </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">جهان</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="همه">همه</a></li>
                                            <li><a href="#" title="مجلس">مجلس</a></li>
                                            <li><a href="#" title="ولایتی">ولایتی</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img44.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img55.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img77.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="نظر شما">نظر شما </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">نظر شما</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="همه">همه</a></li>
                                            <li><a href="#" title="مجلس">مجلس</a></li>
                                            <li><a href="#" title="ولایتی">ولایتی</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="ویدیو">ویدیو </a>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="فرهنگ و هنر">فرهنگ و هنر </a>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="ورزش">ورزش </a>
                        </li>
                        <li>
                            <a href="#" class="watch-link bold" title="WATCH">
                                پخش
                                <span>زنده</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!--  show in mobile    -->
                <div class="top-navigation clearfix show-in-mobile">
                    <ul>
                        <li><a href="#">درباره‌ </a></li>
                        <li><a href="#">تماس با ما</a></li>
                        <li><a href="#">خبرنامه</a></li>
                    </ul>
                    <ul class="language">
                        <li class="bold"><a href="#" class="ltr">English</a></li>
                        <li><a href="#" class="rtl">پشتو</a></li>
                        <li><a href="#" class="rtl">فارسی</a></li>
                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>

    <div class="bar-container">
            <div class="news-bar breaking show">
                <div class="inner-container news">
                    <div class="latest">خبر فوری</div>
                   <a href="#">مصر داوری: رئیس جمهور سابق مرسی مصر زندانی به مدت 20 سال</a> <a href="#" title="" class="close">x</a> </div>
        </div>

    </div>
    </div>
    <div class="news-bar latest" >
            <div class="inner-container news">
                <div class="latest">Latest news</div>
                   <a href="#">مصر داوری: رئیس جمهور سابق مرسی مصر زندانی به مدت 20 سال</a>  </div>
    </div>

</header>