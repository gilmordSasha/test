<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tolonews!</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!--<link rel="stylesheet" type="text/css" href="css/common.css">-->
         <!--<link rel="stylesheet" type="text/css" href="css/home.css">-->
        <!--<link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
    </head>
    <body class="is-exiting">
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header.php'; ?>

        <div id="wrapper" class="wrapper clearfix">

            <!--BLOCKS LAYOUT ONLY MOBILE->-->
            <div class="visible-onlymobile">
                <!--TOP ARTICLE TOLONEWS MOBILE ONLY-->
                <article class="top-post-tolonews clearfix">
                    <a href="#" title=""> <img src="images/img1.jpg" alt="" width="435" height="289" /> </a>
                    <div class="content-top-post-tolonews">
                        <h2 class="grey-dark1 title-top-post-tolonews">
                            <a href="#" title="">Iraq will take back Ramadi 'in days'</a>
                        </h2>
                        <div class="part-left full-width-col-mobile grey-dark4">
                            <p>Iraqi Prime Minister Haider al-Abadi urged government forces to hold fast in Ramadi and prevent Daesh from making further gains, saying they would have air cover and Shiite and militia reinforcements.</p>
                            <span class="post-date grey-light3">
                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                        </div>
                    </div>
                </article>
                <!--TOP ARTICLE TOLONEWS MOBILE ONLY-->
            </div>
            <!--END BLOCKS ONLY MOBILE-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <article class="top-post-tolonews hidden-onmobile clearfix">
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                <a href="#" title="">Iraq will take back Ramadi 'in days'</a>
                            </h2>
                            <div class="part-left full-width-col-mobile grey-dark4">
                                <p>Iraqi Prime Minister Haider al-Abadi urged government forces to hold fast in Ramadi and prevent Daesh from making further gains, saying they would have air cover and Shiite and militia reinforcements.</p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                <div class="related-news hidden-onmobile">
                                    <h3 class="red uppercase-txt">Related news</h3>
                                    <ul>
                                        <li> <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                            <i class="fa fa-angle-right red"></i>
                                            </a>
                                        </li>
                                        <li> <a href="#" title="">MPs Demand Answers On Rumors Of Government Aid In Daesh Hands
                                            <i class="fa fa-angle-right red"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="part-right full-width-col-mobile">
                                <a href="#" title=""> <img src="images/img1.jpg" alt="" width="435" height="289" /> </a>
                            </div>
                        </article>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <!--DEBUT FEATURED NEWS-->
                        <div class="featured clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category  uppercase-txt">Featured news</h2>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">West Imposes New Russia Sanctions, Putin Defiant</a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">WDonec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada...</a> </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">Builders Protest Over Non-Payment By Turkish, US </a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor...</a> </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98" /> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">May </span> 18, 2015 </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                            <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="Load more">Load more</a> </div>
                        </div>
                        <!--END FEATURED NEWS-->
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col">
                        <!--TOP STORIES-->
                        <div class="equal-top-first list-item-widget clearfix bar-bottom hidden-ontablets hidden-onmobile">
                            <h3 class="uppercase-txt line line-red red">Top stories</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">Police Seize Over 1,000kgs Of Opium, 900kgs Hashish
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  $6 Million Dollars Spent So Far To Track 31 Hostages
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> Taliban And Daesh Declare Jihad Against Each Other
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">   Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END TOP STORIES-->
                        <!--6PM BULLETIN VIDEO-->
                        <div class="equal-top box-video hidden-onmobile twoo-column-tablets">
                            <h3 class="line line-grey-dark grey-dark1 uppercase-txt">6pm Bulletin</h3>
                            <div class="box-medium-video grey-box clearfix bar-bottom">
                                <div class="video">
                                    <a href="#" title="video">
                                        <img src="images/video.jpg" alt="" width="280" height="187">
                                        <div class="play"> <i class="medium fa fa-play"></i> </div>
                                    </a>
                                </div>
                                <h3 class="title">Monday 22 May</h3>
                                <div class="text-vds">Directed by James Dupont
                                    <br> Ghani Expresses Confidence on Daesh
                                </div>
                            </div>
                        </div>
                        <!--END 6PM BULLETIN VIDEO-->
                        <!--PUB-->
                        <div class="box grey-box equal-top-pub twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                    </div>
                </div>
            </div>
            <!--WATCH-->
            <div class="row grey-dark marginRow">
                <div class="inner-container row-watch-tolo">
                    <div class="container-top-post line line-grey clearfix">
                        <h2 class="title-category  uppercase-txt">WATCH</h2>
                        <a href="#" title="View all video" class="view-all-post grey-light5">View all video</a>
                    </div>
                    <div class="two-col full-width-col-tablets hidden-onmobile content-watch-tolo">

                        <div class="container-video-cover">
                        <div class="video-hidden hidden">
                                              <iframe width="650" height="365" src="https://www.youtube.com/embed/2L_WTN4OOqc" allowfullscreen=""></iframe>

          </div>
           <div class="block-video-play video">
                            <!--video watch-->

                                <img src="images/img8.jpg" alt="" width="650" height="365" />
                                <i class="big-play fa fa-play play"></i>
                            </div></div>
                             <div class="info">
                                <h3><a href="#" title="Egypt's Morsi Jailed For 20 Years">Egypt's Morsi Jailed For 20 Years</a></h3>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                            </div>

                    </div>
                    <div class="one-col last-col full-width-col-tablets thumbs-watch-tolo full-width-col-mobile">
                        <h3 class="uppercase-txt hidden-onmobile">Top video</h3>
                        <ul>
                            <li class="post-tolonews video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>Kunar Drone Strike Kills Four Taliban</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds1.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>Obama Says US Not Losing Against Daesh</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews  hidden-onmobile video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>Kunar Drone Strike Kills Four Taliban</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </a>
                            </li>
                            <li class="post-tolonews hidden-onmobile video-thumbnail small-column">
                                <a title="" href="#">
                                    <div class="container-image"> <img src="images/img_vds1.jpg" alt="" width="100" height="67" /> <i class="small-play fa fa-play"></i> </div>
                                    <div class="content-article">
                                        <p>Obama Says US Not Losing Against Daesh</p>
                                        <span class="post-date grey-light3 time grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END WATCH-->
            <div class="row">
                <div class="inner-container">
                    <div class="one-col full-width-col-tablets hidden-onmobile">
                        <!--MOST READ / MOST WATCHED TABULATION-->
                        <div class="list-item-widget twoo-column-tablets bar-bottom">
                            <ul class="tabulations-tolo container-top-post no-margin line-red red">
                                <li class="titleBlock uppercase-txt"> <a class="active" href="#most-read">Most read</a> </li>
                                <li class="grey-light5 uppercase-txt"> <a href="#most-watched">Most watched</a> </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-read">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">Police Seize Over 1,000kgs Of Opium, 900kgs Hashish
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  $6 Million Dollars Spent So Far To Track 31 Hostages
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> Taliban And Daesh Declare Jihad Against Each Other
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                            <ul class="grey-box list-item" id="most-watched">
                                <li>
                                    <a href="#"> <span class="number grey-light3">6</span> <span class="content">Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">7</span> <span class="content">Police Seize Over 1,000kgs Of Opium, 900kgs Hashish
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">8</span> <span class="content">  $6 Million Dollars Spent So Far To Track 31 Hostages
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">9</span> <span class="content"> Taliban And Daesh Declare Jihad Against Each Other
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">10</span> <span class="content">   Blast In Kandahar Lives 2 Dead And 11 Injured
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END MOST READ / MOST WATCHED TABULATION-->
                        <!--WEATHER-->
                        <div class="box-weather twoo-column-tablets last">
                            <h3 class="line bold uppercase-txt">Weather</h3>
                            <div class="container-weather">
                        <div class="image-weather"><img src="images/image-meteo.jpg" alt="" width="310" height="289" /></div>
                             <a href="http://www.accuweather.com/en/en/kabul/4361/weather-forecast/4361" class="aw-widget-legal">
                                    <!--
                                    By accessing and/or using this code snippet, you agree to AccuWeather's terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather's Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                                    -->
                                    </a>
                                    <div id="awcc1455525954273" class="aw-widget-current"  data-locationkey="4361" data-unit="c" data-language="en" data-useip="false" data-uid="awcc1455525954273"></div><script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>

                        </div>
                        </div>
                        <!--END WEATHER-->
                    </div>
                    <!--BUSINESS-->
                    <div class="two-col last-col full-width-col-tablets full-width-col-mobile">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">Business</h2>
                            <a title="View all Business news" href="#" class="view-all-post grey-light5">View all Business news</a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img22.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Merkel, Hollande Tell Greece to Take Route A to Agreement</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img33.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Millions of Barrels of Oil Are About to Vanish</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img44.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Japan Still Beating China on One Score: World's Top Creditor</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img55.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Rigs Running Hot Offshore as Shale Scales Back</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/img66.png" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Iraq PM Haider al-Abadi: Charlie Rose</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image"> <img src="images/img77.png" alt="" width="147" height="98"> </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Imperial Tobacco Profit Rises Despite Impact of Iraq Conflict</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="Load more">Load more</a> </div>
                    </div>
                    <!--END BUSINESS-->
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="one-col last-col full-width-col-tablets right-col full-width-col-mobile">
                        <!--CURRENCY-->
                        <div class="currency bar-bottom twoo-column-tablets hidden-onmobile">
							<?php
							$url['USD'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNUSD&f=USD&v=1&do=cvals&ln=fr';
							$url['EUR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNEUR&f=EUR&v=1&do=cvals&ln=fr';
							$url['INR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNINR&f=INR&v=1&do=cvals&ln=fr';
							$url['PKR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNPKR&f=PKR&v=1&do=cvals&ln=fr';
							$url['IRR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNIRR&f=IRR&v=1&do=cvals&ln=fr';
							$url['AED'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNAED&f=AED&v=1&do=cvals&ln=fr';

							setlocale(LC_MONETARY, 'en_US');
							foreach($url as $currency => $link){
								$obj = json_decode(file_get_contents($link), true);
								$Res[$currency]= $obj["AFN"];
							}
							// print_r($Res);
							?>

                            <div class="container-top-post no-margin line line-red red clearfix">
                                <h3 class="titleBlock uppercase-txt">Currency rates</h3>
                                <span class="grey-light5 view-all-post">Last report: 05.20.15</span>
                            </div>
                            <table class="grey-box">
                                <tbody>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-us"></td>
                                        <td class="symbol">1 USD</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['USD']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-eu"></td>
                                        <td class="symbol">1 Euro</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['EUR']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-in"></td>
                                        <td class="symbol">1000 INR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['INR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-pk"></td>
                                        <td class="symbol">1000 PKR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['PKR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ir"></td>
                                        <td class="symbol">1000 IRR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['IRR']*1000 ); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ae"></td>
                                        <td class="symbol">1 AED</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['AED']); ?>AFN</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--END CURRENCY-->
                        <!--PUB 2-->
                        <div class="box equal-top-pub pub grey-box twoo-column-tablets last">
                            <a href="#" title=""> <img src="images/pub2.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB 2-->
                    </div>
                    <!--CATEGROY SPORT-->
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">Sport</h2>
                            <a href="#" title="View all Sport news" class="view-all-post grey-light5">View all Sport news</a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport1.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Volleyball Team Flies Out To Bangladesh for Asian Contest</a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews bar-bottom">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport2.jpg" alt="" width="310" height="206" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Afghan Football Team to Host World Cup Qualifier Matches in </a>
                                        </h3>
                                        <p class="hidden-onmobile"> <a href="#" title="">Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper. Curabitur blandit tempus porttitor… </a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small1.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Kandahar Football Federation Chief Accused of Embezzlement</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small2.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">U-16 Futsal Premier League Kicks Off in Kabul</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small3.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">Handball Friendly Cup Kicks-Off in Kabul</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                            <li class="one-col last-col">
                                <article class="post-tolonews small-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/sport-small4.jpg" alt="" width="147" height="98" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h4>
                                            <a href="#" title="">National Football Team Heads Abroad to Prepare for World Cup Qualifier</a>
                                        </h4>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">May </span> 18, 2015 </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="Load more">Load more</a> </div>
                        <!--END SPORT-->
                    </div>
                    <!--CATEGROY WORLD-->
                    <div class="four-col twoo-col-ontablets last-col marginRow">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">WORLD</h2>
                            <a href="#" title="View all World news" class="view-all-post grey-light5">View all World news</a>
                        </div>
                        <ul class="row full-width-col-mobile list-post">
                            <li class="one-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">800 Dead After Sunday's Mediterranean Migrant Shipwreck</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">A number of villagers from Malistan, Ajristan, Jaghori, Nawa, Nawur and Muqur districts of Ghazni province have been...</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="two-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world1.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Yemen Rebel Leader Vows Resistance Against Saudi-led Air</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. Donec ullamcorper nulla non metus…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="three-col">
                                <article class="post-tolonews bar-bottom medium-column">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world2.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Saudis Pledge to Fund Aid for Yemen, Keep Up Air Raids</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna meu leo…</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                            <li class="four-col last-col">
                                <article class="post-tolonews bar-bottom medium-column last">
                                    <div class="container-image">
                                        <a href="#" title=""> <img src="images/world.jpg" alt="" width="225" height="150" /> </a>
                                    </div>
                                    <div class="content-article">
                                        <h3 class="title-article">
                                            <a href="#" title="">Al-Qaeda Seizes Southeast Yemen Airport</a>
                                        </h3>
                                        <p class="hidden-ontablets hidden-onmobile"> <a href="#" title="">A number of villagers from Malistan, Ajristan, Jaghori, Nawa, Nawur and Muqur districts of Ghazni province have been...</a> </p>
                                        <span class="post-date grey-light3">
                                        <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                    </div>
                                </article>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="Load more">Load more</a> </div>
                    </div>
                    <!--END WORLD-->
                </div>
            </div>
            <div class="row grey-dark marginRow hidden-onmobile">
                <!--SOCIAL SHARE-->
                <div class="inner-container row-social-tolo">
                    <h2 class="uppercase-txt line line-grey2 ">Follow us on social networks</h2>
                    <ul class="social-media">
                        <li>
                            <a href="https://www.facebook.com/TOLOnews" target="_blank" title="Facebook"> <i class="fa fa-facebook"></i> Facebook </a>
                        </li>
                        <li>
                            <a href="http://www.twitter.com/TOLOnews" target="_blank" title="Twitter"> <i class="fa fa-twitter"></i>Twitter </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+TOLOnews" target="_blank" title="Google+"> <i class="fa fa-google-plus"></i> Google+ </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="Youtube"> <i class="fa fa-youtube-play"></i> Youtube </a>
                        </li>
                        <li class="last">
                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="Instagram"> <i class="fa fa-instagram"></i> Instagram </a>
                        </li>
                    </ul>
                </div>
                <!--END SOCIAL SHARE-->
            </div>
            <div class="row marginRow">
                <div class="inner-container">
                    <div class="one-col full-width-col-tablets">
                        <!--TOLONEWS POLL-->
                        <div class="twoo-column-tablets hidden-onmobile">
                            <h3 class="line red uppercase-txt line-red">Tolonews Poll</h3>
                            <div class="grey-box box-vote bar-bottom">
                                <div class="title-category">Will the agreement between the two presidential candidates solve the election issue for announcing of final result?</div>
                                <form accept-charset="UTF-8" method="post" action="/">
                                    <ul>
                                        <li>
                                        	<div class="question">
                                                <input id="choice1" type="radio" name="vote" value="Yes" />
                                                <label for="choice1">Yes </label>
                                            </div>
                                            <div class="result">
                                           		<span>Yes </span><span class="red">(90%)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="question">
                                                <input id="choice2" type="radio" name="vote" value="No" />
                                                <label for="choice2">No </label>
                                              </div>
                                              <div class="result">
                                           		<span>No  </span><span class="red">(9%)</span>
                                            </div>
                                        </li>
                                        <li>
                                        	<div class="question">
                                                <input id="choice3" type="radio" name="vote" value="No" />
                                                <label for="choice3">I don't know </label>
                                            </div>
                                            <div class="result">
                                           		<span>I don't know </span><span class="red">(1%)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <input type="submit" class="link-square red button" value="Vote now" /> <a href="#" title="View results" class="link-square view-results">View results</a>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                        <!--END TOLONEWS POLL-->
                        <!-- TOLONEWS SCHEDULE-->
                        <div class="box-tolonews-program bar-bottom hidden-onmobile twoo-column-tablets last">
                            <h3 class="line green uppercase-txt line-green">Tolonews Schedule</h3>
                            <ul class="green-light">
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">On air</span> <span> World Update</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">To follow </span> <span> TOLO News</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>The lastest five minute news bulletin from Tolo News</p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> World Update</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span> World Update</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">On air</span> <span> World Update</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                    </div>
                                    <p>Jordan tackles Islamists; 19 countries in 24h; a four day working week? </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                        <span class="red">To follow </span> <span> TOLO News</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                    </div>
                                    <p>The lastest five minute news bulletin from Tolo News</p>
                                </li>
                                <li class="schedule-link"> <a class="tolo-link green" rel="nofollow">View Today's schedule</a> </li>
                            </ul>
                        </div>
                        <!--END TOLONEWS SCHEDULE-->
                    </div>
                    <!-- PHOTOS-->
                    <div class="two-col full-width-col-tablets last-col hidden-onmobile box-photo-right">
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">Photos of the day</h2>
                            <a title="View all Photos" href="#" class="view-all-post grey-light5">View all Photos</a>
                        </div>
                        <div id="panel">
                            <img src="images/image_01_large.jpg" alt="" width="650" height="434" class="container-image" id="largeImage" />
                            <div id="description" class="bar-bottom title-description">Winter in Afghanistan</div>
                        </div>
                        <div class="thumbs-photo">
                            <div class="thumbs-medium-column"> <img src="images/image_01_thumb.jpg" alt="Mazar-e-Sharif" width="206" height="137" /> <span class="title-thumb">Mazar-e-Sharif</span> </div>
                            <div class="thumbs-medium-column"> <img src="images/image_02_thumb.jpg" alt="North of Afghanistan" width="206" height="138" /> <span class="title-thumb">North of Afghanistan</span> </div>
                            <div class="thumbs-medium-column last"> <img src="images/image_03_thumb.jpg" alt="Winter in Afghanistan" width="206" height="138" /> <span class="title-thumb">Winter in Afghanistan</span> </div>
                        </div>
                    </div>
                    <!-- END PHOTOS-->
                </div>
            </div>
            <div class="row hidden-onmobile">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90" /> </a>
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <!-- OPINIONS-->
                        <div class="container-top-post line line-grey clearfix">
                            <h2 class="title-category  uppercase-txt">Opinions</h2>
                            <a title="View all Opinions" href="#" class="view-all-post grey-light5">View all Opinions</a>
                        </div>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">Farkhunda's Murder: A National Tragedy </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">Children at work: Millions of children around the world must work for food and shelter </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna meu leo…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150" /> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">Ghani and Abdullah Take Washington </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">Donec sed odio dui. Donec id elit non mi porta gravi at eget metus. Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada…</a> </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">Today</span> - 03:15 PM </span>
                                </div>
                            </li>
                        </ul>
                        <div class="load-more-post centered uppercase-txt visible-onlymobile"> <a href="#" class="red bold" title="Load more">Load more</a> </div>
                        <!-- END OPINIONS-->
                    </div>
                    <div class="one-col last-col noTopMargin-col">
                        <!-- TWITTER TOLONEWS-->
                        <div class="box box-tolo-twitter hidden-onmobile  hidden-ontablets">
                            <div class="container-top-post no-margin line line-blue clearfix">
                                <h3 class="titleBlock blue1 uppercase-txt">Twitter feed</h3>
                                <a title="Tolonews" target="_blank" href="https://twitter.com/TOLOnews" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#43adef"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="light"
                                    data-border-color="#e9e9e9"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a data-show-count="false"  href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="blue1" target="back"> <i class="fa fa-twitter"></i> <span>Follow @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!-- END TWITTER TOLONEWS-->
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer.php'; ?>
        </div>
    </body>
</html>
