<!DOCTYPE html>
<html lang="en">
    <head>
        <title>طلوع نیوز</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!--<link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <link rel="stylesheet" href="css/rtl-app.css">
    </head>
    <body class="is-exiting">
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header-af.php'; ?>
        <div id="wrapper" class="wrapper clearfix">
            <!--TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="visible-onlymobile top-post-tolonews post-single clearfix">
                <article class="top-post-tolonews clearfix">
                    <a href="#" title=""> <img src="images/img8.jpg" alt="" width="435" height="289"> </a>
                    <div class="content-top-post-tolonews">
                        <h1 class="red category-title no-margin">افغانستان</h1>
                        <h2 class="grey-dark1 title-top-post-tolonews">
                            <a href="#" title="">مرسی مصر زندانی به مدت 20 سال</a>
                        </h2>
                        <div class=" full-width-col-mobile grey-dark4">
                            <p>حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است. حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است.</p>
                            <span class="post-date grey-light3">
                            <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                        </div>
                    </div>
                </article>
            </div>
            <!--END TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <article class="top-post-tolonews hidden-onmobile clearfix">
                            <h1 class="red category-title no-margin">افغانستان</h1>
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                <a href="#" title="">مرسی مصر زندانی به مدت 20 سال</a>
                            </h2>
                            <div class="part-left full-width-col-mobile grey-dark4">
                                <p>حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است. حیدر العبادی، نخست وزیر عراق نیروهای دولتی خواست تا سریع در رمادی و جلوگیری از داعش از دستاوردهای بیشتر، گفت: آنها می پوشش هوایی و تقویت شیعه و نیروهای شبه نظامی است.</p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                            <div class="part-right full-width-col-mobile">
                                <a href="#" title=""> <img src="images/img8.jpg" alt="" width="435" height="289"> </a>
                            </div>
                        </article>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                        <div class="featured clearfix">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">آخرین اخبار سیاست</h2>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                            </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews bar-bottom">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h3 class="title-article">
                                                <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا </a>
                                            </h3>
                                            <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است...</a>
                                            </p>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-video"></i>غنی، روحانی تمرکز بر امنیت و ریشه کن کردن مواد مخدر
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">
                                                <i class="icon-post icon-news"></i>کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان
                                                </a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">کشته شدن 14 شورشی کشته شدند در عملیات نیروهای امنیت ملی افغانستان</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="one-col top-equalBlock full-width-col-tablets full-width-col-mobile last-col">
                        <!--RELATED STORIES-->
                        <div class="twoo-column-tablets hidden-onmobile list-item-widget clearfix bar-bottom">
                            <h3 class="uppercase-txt line line-red red">داستان های مرتبط</h3>
                            <ul class="grey-box list-item">
                                <li>
                                    <a href="#"> <span class="number grey-light3">1</span> <span class="content">انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">2</span> <span class="content">پلیس بیش از 1،000kgs تریاک به دست گرفتن، 900kgs حشیش
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">3</span> <span class="content">  6 میلیون $ دلار صرف تا کنون به پیگیری 31 گروگان
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">4</span> <span class="content"> طالبان و داعش اعلام جهاد بر علیه یکدیگر
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                                <li>
                                    <a href="#"> <span class="number grey-light3">5</span> <span class="content">   انفجار در قندهار زندگی 2 نیز کشته و 11 زخمی
                                    <i class="fa fa-angle-right red"></i>
                                    </span> </a>
                                </li>
                            </ul>
                        </div>
                        <!--END RELATED STORIES-->
                        <!--PUB-->
                        <div class="box equal-top grey-box  twoo-column-tablets last pub">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250"> </a>
                        </div>
                        <!--END PUB-->
                        <!--SOCIAL SIDEBAR-->
                        <div class="social-sidebar equal-top-margin twoo-column-tablets hidden-onmobile clearfix">
                            <h3 class="line line-grey3 grey-dark5 uppercase-txt">ما را دنبال کنید  </h3>
                            <div class="social-content post-tolonews">
                                <div class="box-social">
                                    <h3 class="title-article white">ما در شبکه های اجتماعی عضویت</h3>
                                    <ul class="social-media">
                                        <li>
                                            <a href="https://www.facebook.com/TOLOnews" target="_blank" title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://www.twitter.com/TOLOnews" target="_blank" title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/+TOLOnews" target="_blank" title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                    <div class="col-newsletter full-width-col-mobile">
                                        <h3 class="title-article white">خبرنامه</h3>
                                        <p>ثبت نام برای دریافت بهترین طلوع نیوز روزانه</p>
                                        <form action="#" method="post">
                                            <label class="hide" for="email">آدرس ایمیل شما </label>
                                            <input type="email" placeholder="آدرس ایمیل شما" name="email" id="email">
                                            <input type="submit" value="موافقم">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End SOCIAL SIDEBAR-->
                        <!--CURRENCY-->
						<div class="currency bar-bottom twoo-column-tablets hidden last show-onlytablets">
							<?php
							$url['USD'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNUSD&f=USD&v=1&do=cvals&ln=fr';
							$url['EUR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNEUR&f=EUR&v=1&do=cvals&ln=fr';
							$url['INR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNINR&f=INR&v=1&do=cvals&ln=fr';
							$url['PKR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNPKR&f=PKR&v=1&do=cvals&ln=fr';
							$url['IRR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNIRR&f=IRR&v=1&do=cvals&ln=fr';
							$url['AED'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNAED&f=AED&v=1&do=cvals&ln=fr';

							setlocale(LC_MONETARY, 'en_US');
							foreach($url as $currency => $link){
								$obj = json_decode(file_get_contents($link), true);
								$Res[$currency]= $obj["AFN"];
							}
							// print_r($Res);
							?>

                            <div class="container-top-post no-margin line line-red red clearfix">
                                <h3 class="titleBlock uppercase-txt">نرخ ارز</h3>
                                <span class="grey-light5 view-all-post">آخرین خبر: 05.20.15</span>
                            </div>
                            <table class="grey-box">
                                <tbody>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-us"></td>
                                        <td class="symbol">1 USD</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['USD']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-eu"></td>
                                        <td class="symbol">1 Euro</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['EUR']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-in"></td>
                                        <td class="symbol">1000 INR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['INR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-pk"></td>
                                        <td class="symbol">1000 PKR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['PKR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ir"></td>
                                        <td class="symbol">1000 IRR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['IRR']*1000 ); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ae"></td>
                                        <td class="symbol">1 AED</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['AED']); ?>AFN</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--END CURRENCY-->
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="two-col full-width-col-mobile full-width-col-tablets">
                        <!--More political news-->
                        <div class="two-col last-col full-width-col-tablets full-width-col-mobile">
                            <div class="container-top-post line line-grey">
                                <h2 class="title-category red uppercase-txt">اخبار سیاسی بیشتر</h2>
                                <a href="#" title="View all" class="view-all-post grey-light5">مشاهده همه</a>
                            </div>
                            <ul class="row full-width-col-mobile list-post">
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">ژاپن هنوز ضرب و شتم چین در یکی امتیاز: بستانکار برتر جهان</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">چندمنظوره در حال اجرا داغ دریایی شیل فلس برگشت</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img5.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">عراق حیدر العبادی: چارلی رز</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image"> <img src="images/img7.png" alt="" width="147" height="98"> </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">IMPERIAL TOBACCO سود افزایش با وجود تاثیر جنگ عراق</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img6.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">ژاپن هنوز ضرب و شتم چین در یکی امتیاز: بستانکار برتر جهان</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">چندمنظوره در حال اجرا داغ دریایی شیل فلس برگشت</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img7.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">عراق حیدر العبادی: چارلی رز</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image"> <img src="images/img5.png" alt="" width="147" height="98"> </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">IMPERIAL TOBACCO سود افزایش با وجود تاثیر جنگ عراق</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image">
                                            <a href="#" title=""> <img src="images/img4.png" alt="" width="147" height="98"> </a>
                                        </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">عراق حیدر العبادی: چارلی رز</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                                <li class="one-col last-col">
                                    <article class="post-tolonews small-column">
                                        <div class="container-image"> <img src="images/img6.png" alt="" width="147" height="98"> </div>
                                        <div class="content-article">
                                            <h4>
                                                <a href="#" title="">IMPERIAL TOBACCO سود افزایش با وجود تاثیر جنگ عراق</a>
                                            </h4>
                                            <span class="post-date grey-light3">
                                            <span class="uppercase-txt">ماه می </span> ۱۸، ۲۰۱۵ </span>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                        <!--END More political news-->
                    </div>
                    <div class="one-col hidden-onmobile hidden-ontablets last-col">
                        <!--CURRENCY-->
                        <div class="currency bar-bottom">
							<?php
							$url['USD'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNUSD&f=USD&v=1&do=cvals&ln=fr';
							$url['EUR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNEUR&f=EUR&v=1&do=cvals&ln=fr';
							$url['INR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNINR&f=INR&v=1&do=cvals&ln=fr';
							$url['PKR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNPKR&f=PKR&v=1&do=cvals&ln=fr';
							$url['IRR'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNIRR&f=IRR&v=1&do=cvals&ln=fr';
							$url['AED'] = 'http://freecurrencyrates.com/api/action.php?s=fcr&iso=AFNAED&f=AED&v=1&do=cvals&ln=fr';

							setlocale(LC_MONETARY, 'en_US');
							foreach($url as $currency => $link){
								$obj = json_decode(file_get_contents($link), true);
								$Res[$currency]= $obj["AFN"];
							}
							// print_r($Res);
							?>

                            <div class="container-top-post no-margin line line-red red clearfix">
                                <h3 class="titleBlock uppercase-txt">نرخ ارز</h3>
                                <span class="grey-light5 view-all-post">آخرین خبر: 05.20.15</span>
                            </div>
                            <table class="grey-box">
                                <tbody>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-us"></td>
                                        <td class="symbol">1 USD</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['USD']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-eu"></td>
                                        <td class="symbol">1 Euro</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['EUR']); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-in"></td>
                                        <td class="symbol">1000 INR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['INR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-pk"></td>
                                        <td class="symbol">1000 PKR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['PKR']*1000); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ir"></td>
                                        <td class="symbol">1000 IRR</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['IRR']*1000 ); ?>AFN</td>
                                    </tr>
                                    <tr>
                                    	<td class="space" width="20" height="20"></td>
                                        <td class="first flag-icon flag-icon-ae"></td>
                                        <td class="symbol">1 AED</td>
                                        <td class="price"><?php  echo money_format('%(#10n', $Res['AED']); ?>AFN</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--END CURRENCY-->
                    </div>
                </div>
                <div class="inner-container fullWidth-pub  hidden-onmobile marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90"> </a>
                </div>
                <div class="post-category  row clearfix inline-three-column">
                    <div class="container-top-post line line-grey">
                        <h2 class="title-category uppercase-txt">همچنین در اخبار</h2>
                    </div>
                    <div class="column list-post one-col">
                        <article class="post-tolonews">
                            <div class="container-image">
                                <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                                <div class="tag-category"><a href="#" title="" class="type-category">بازرگانی</a></div>
                            </div>
                            <div class="content-article">
                                <h3 class="title-article">
                                    <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                </h3>
                                <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                </p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </article>
                    </div>
                    <div class="column list-post one-col">
                        <article class="post-tolonews">
                            <div class="container-image">
                                <a href="#" title=""> <img src="images/img3.jpg" alt="" width="310" height="206"> </a>
                                <div class="tag-category"><span class="type-category">بازرگانی</span></div>
                            </div>
                            <div class="content-article">
                                <h3 class="title-article">
                                    <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                </h3>
                                <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                </p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </article>
                    </div>
                    <div class="column list-post one-col last-col">
                        <article class="post-tolonews">
                            <div class="container-image">
                                <a href="#" title=""> <img src="images/img4.jpg" alt="" width="310" height="206"> </a>
                                <div class="tag-category"><span class="type-category">بازرگانی</span></div>
                            </div>
                            <div class="content-article">
                                <h3 class="title-article">
                                    <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                </h3>
                                <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                </p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </article>
                    </div>
                    <!--DUPLIATE BLOCK /TOLONEWS SCHEDULE BLOCK ONLY TABLETS-->
                    <div class="one-col box-tolonews-program bar-bottom column show-onlytablets hidden">
                        <h3 class="line green uppercase-txt line-green">جدول طلوع نیوز</h3>
                            <ul class="green-light">
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li class="active">
                                     <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                </div>
                                <p>آخرین بولتن خبری پنج دقیقه از طلوع نیوز</p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                   <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                </li>
                                <li>
                                   <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li class="schedule-link"> <a class="tolo-link green">برنامه های امروز ما</a> </li>
                            </ul>
                    </div>
                </div>
                <!--TOLONEWS SCHEDULE DESKTOP-->
                <div class="post-category row clearfix inline-three-column hidden-ontablets hidden-onmobile">
                    <div class="one-col box-tolonews-program bar-bottom column">
                        <h3 class="line green uppercase-txt line-green">جدول طلوع نیوز</h3>
                            <ul class="green-light">
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li class="active">
                                     <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                </div>
                                <p>آخرین بولتن خبری پنج دقیقه از طلوع نیوز</p>
                                </li>
                                <li class="active">
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                    <div class="tolo-title"> <span class="Boldtitle">
                                    <span> به روز رسانی جهانی</span> </span> <span class="time">11:30 - 11:55 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li>
                                   <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">ما را دنبال کنید </span> <span> طلوع نیوز</span> </span> <span class="time">11:00 - 11:05 GMT</span>
                                </li>
                                <li>
                                   <div class="tolo-title"> <span class="Boldtitle">
                                    <span class="red">در هوا</span> <span> به روز رسانی جهانی</span> </span> <span class="time">09:05 - 11:00 GMT</span>
                                </div>
                                <p>اردن خاکستر اسلامگرایان؛ 19 کشور در 24h؛ یک هفته کار چهار روز؟ </p>
                                </li>
                                <li class="schedule-link"> <a class="tolo-link green">برنامه های امروز ما</a> </li>
                            </ul>
                    </div>
                    <div class="column list-post one-col">
                        <div class="container-top-post line line-grey">
                            <h2 class="title-category uppercase-txt">ورزش</h2>
                            <a href="#" title="View all" class="view-all-post grey-light5">مشاهده همه</a>
                        </div>
                        <article class="post-tolonews">
                            <div class="container-image">
                                <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                            </div>
                            <div class="content-article">
                                <h3 class="title-article">
                                    <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                </h3>
                                <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                </p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </article>
                    </div>
                    <div class="column list-post one-col last-col">
                        <div class="container-top-post line line-grey">
                            <h2 class="title-category uppercase-txt">فرهنگ و هنر </h2>
                            <a href="#" title="View all" class="view-all-post grey-light5">مشاهده همه</a>
                        </div>
                        <article class="post-tolonews">
                            <div class="container-image">
                                <a href="#" title=""> <img src="images/img2.jpg" alt="" width="310" height="206"> </a>
                            </div>
                            <div class="content-article">
                                <h3 class="title-article">
                                    <a href="#" title="">غرب تحمیل روسیه جدید تحریم، پوتین مبارز</a>
                                </h3>
                                <p class="hidden-onmobile"> <a href="#" title="">سازندگان اعتراض به عدم پرداخت توسط ترکیه، آمریکا...</a>
                                </p>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            <div class="row hidden-onmobile">
                <div class="inner-container fullWidth-pub  marginRow centered">
                    <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90"> </a>
                </div>
            </div>
            <div class="row">
                <div class="inner-container">
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <!-- OPINIONS-->
                        <div class="container-top-post line line-grey">
                            <h2 class="title-category  uppercase-txt">نظریات</h2>
                            <a title="View all Opinions" href="#" class="view-all-post grey-light5">تماشای همه نظریات</a>
                        </div>
                        <ul class="list-post">
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_1.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">قتل فرخنده است: یک تراژدی ملی </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_2.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">کودکان در محل کار: میلیون ها نفر از کودکان در سراسر جهان باید برای غذا و سرپناه کار </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                            <li class="post-tolonews bar-bottom vertical-bloc last">
                                <div class="container-image">
                                    <a href="#" title=""> <img src="images/opp_3.jpg" alt="" width="225" height="150"> </a>
                                </div>
                                <div class="content-article">
                                    <h3 class="title-article">
                                        <a href="#" title="">غنی و عبدالله نگاهی واشنگتن </a>
                                    </h3>
                                    <p class="hidden-onmobile"> <a href="#" title="">تعدادی از روستاییان از مالستان، ولسوالی اجرستان، جاغوری، ناوه، ناوه و ولسوالی مقر ولایت غزنی بوده است…</a>
                                    </p>
                                    <span class="post-date grey-light3">
                                    <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                </div>
                            </li>
                        </ul>
                        <!-- END OPINIONS-->
                    </div>
                    <div class="one-col last-col noTopMargin-col">
                        <!-- TWITTER TOLONEWS-->
                        <div class="box box-tolo-twitter hidden-onmobile  hidden-ontablets last">
                            <div class="container-top-post no-margin line line-blue">
                                <h3 class="titleBlock blue1 uppercase-txt">توئیتر طلوع نیوز</h3>
                                <a href="https://twitter.com/TOLOnews"  target="_blank" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#43adef"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="light"
                                    data-border-color="#e9e9e9"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a data-show-count="false" href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="blue1" target="back"> <i class="fa fa-twitter"></i> <span>را دنبال کنید @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!-- END TWITTER TOLONEWS-->
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer-af.php'; ?>
        </div>
    </body>
</html>
