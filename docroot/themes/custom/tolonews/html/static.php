<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tolonews</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!-- <link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/home.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <!-- javascripts TOLONEWS -->
<!--external JS-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/external/jquery.uniform.js"></script>
<!--TOLO JS -->
<script src="js/common.js"></script>
<!--SHARE THIS JS -->

<!--<script src="js/min.js"></script>-->

<!--TOLO CSS -->
<!--English version CSS FILE-->
<link rel="stylesheet" href="css/ltr-app.css">
<!--Other languages RTL CSS
<link rel="stylesheet" href="css/rtl-app.css">-->
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    </head>
    <!--PAGE SINGLE-->
    <body class="page-single is-exiting">
        <div class="overlay visible-onlymobile "></div>
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <header>
    <div class="group-top hidden-onmobile">
        <div class="inner-container">
            <img src="images/pub-top.jpg" alt="pub top" width="728" height="90" />
        </div>
    </div>
    <div class="navigation-sticky">
        <div class=" group-middle clearfix">
  			<div class="inner-container">
            <div class="logo">
                <h1>
                    <a title="Accueil" href="/">
                        <img src="images/logo.png" alt="logo" width="91" height="91" />
                    </a>
            </h1>
            </div>
            <button type="button" class="navbar-toggle">
            <span>MENU</span>
            </button>
            <div class="container-header">
                <div class="header-right clearfix">
                    <div class="search">
                        <form class="form-search-header" accept-charset="UTF-8" method="post" action="/">
                            <label class="hide" for="edit-search">Rechercher </label>
                            <input type="submit" class="form-submit" id="search-submit">
                             <input id="edit-search" type="text" placeholder="Search on TOLOnews">
                        </form>
                    </div>
                    <div class="top-navigation clearfix">
                        <ul>
                            <li><a href="archives.php">About</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Newsletter</a></li>
                        </ul>
                        <ul class="language">
                            <li class="bold"><a href="#" class="ltr">English</a></li>
                            <li><a href="#" class="rtl">پشتو</a></li>
                            <li><a href="#" class="rtl">فارسی</a></li>
                        </ul>
                    </div>
                </div>
                <nav>
                    <ul class="nav navbar-nav clearfix">
                        <li class="lvl1"><a href="#" title="Home">Home</a></li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="Afghanistan">Afghanistan</a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">Afghanistan</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="">All</a></li>
                                            <li><a href="#" title="">PARLIAMENT</a></li>
                                            <li><a href="#" title="">PROVINCIAL</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="Business">Business </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">Business</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="All">All</a></li>
                                            <li><a href="#" title="PARLIAMENT">PARLIAMENT</a></li>
                                            <li><a href="#" title="PROVINCIAL">PROVINCIAL</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img44.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img55.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img77.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="Election">Election </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">Election</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="All">All</a></li>
                                            <li><a href="#" title="PARLIAMENT">PARLIAMENT</a></li>
                                            <li><a href="#" title="PROVINCIAL">PROVINCIAL</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="World">World </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">World</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="All">All</a></li>
                                            <li><a href="#" title="PARLIAMENT">PARLIAMENT</a></li>
                                            <li><a href="#" title="PROVINCIAL">PROVINCIAL</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img44.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img55.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img77.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" class="dropdown-toggle" title="Opinion">Opinion </a>
                            <div class="dropdown-menu">
                                <div class="inner-container">
                                    <div class="one-col">
                                        <h2 class="red">Opinion</h2>
                                        <ul class="list-menu">
                                            <li><a href="#" title="All">All</a></li>
                                            <li><a href="#" title="PARLIAMENT">PARLIAMENT</a></li>
                                            <li><a href="#" title="PROVINCIAL">PROVINCIAL</a></li>
                                        </ul>
                                    </div>
                                    <div class="two-col last-col">
                                        <ul class="row full-width-col-mobile list-post ">
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img4.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">Ghani, Rouhani Focus on Security and Rooting Out Drugs</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img6.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-video"></i>Ghani, Rouhani Focus on Security and Rooting Out Drugs
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img5.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">
                                                            <i class="icon-post icon-news"></i>14 Insurgents Killed in ANSF Operations
                                                        </a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                            <li class="one-col last-col">
                                                <article class="post-tolonews small-column">
                                                    <div class="container-image">
                                                        <a href="#" title="">
                                                            <img src="images/img7.png" alt="" width="147" height="98" />
                                                        </a>
                                                    </div>
                                                    <div class="content-article">
                                                        <h4>
                                                        <a href="#" title="">14 Insurgents Killed in ANSF Operations</a>
                                                        </h4>
                                                        <span class="post-date grey-light3">
                                                            <span class="uppercase-txt">May </span> 18, 2015
                                                        </span>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="Video">Video </a>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="Arts &amp; Culture">Arts &amp; Culture </a>
                        </li>
                        <li class="lvl1">
                            <a href="#" title="Sports">Sports </a>
                        </li>
                        <li>
                            <a href="#" class="watch-link bold" title="WATCH">
                                WATCH
                                <span>LIVE</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!--  show in mobile    -->
                <div class="top-navigation clearfix show-in-mobile">
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Newsletter</a></li>
                    </ul>
                    <ul class="language">
                        <li class="bold"><a href="#" class="ltr">English</a></li>
                        <li><a href="#" class="rtl">پشتو</a></li>
                        <li><a href="#" class="rtl">فارسی</a></li>
                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
      <div class="bar-container">
            <div class="news-bar breaking show">
                <div class="inner-container news">
                    <div class="latest">Breaking news</div>
                   <a href="#">EGYPT JUGEMENT: Egypt's former President Morsi jailed for 20 years</a> <a href="#" title="" class="close">x</a> </div>
        </div>

    </div>
    </div>
    <div class="news-bar latest" >
            <div class="inner-container news">
                <div class="latest">Latest news</div>
                <div class="news-items">
                    <a href="#" class="active">EGYPT JUGEMENT: Egypt's former President Morsi jailed for 20 years</a>
                    <a href="#">EGYPT JUGEMENT 2: Egypt's former President Morsi jailed for 20 years</a>
                    <a href="#">EGYPT JUGEMENT 3: Egypt's former President Morsi jailed for 20 years</a>
                </div>
            </div>
    </div>
</header>
        <div id="wrapper" class="wrapper clearfix" style="padding-top: 180px;">
            <!--TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="visible-onlymobile top-post-tolonews post-single clearfix">
                <article>
                    <div class="content-top-post-tolonews">
                        <div class="entry-headder">
                            
                            <h2 class="grey-dark1 title-top-post-tolonews">
                                Four Killed, Over 60 Injured in Zabul Suicide Bombing
                            </h2>
                        </div>
                        
                        <div class="intro part-left full-width-col-mobile">
                            <p>At least four civilians were killed and more than 60 people - including children and women - were injured in a suicide attack in southern Zabul province on Monday, local officials said.</p>
                        </div>
                        <div class="entry-summary clearfix">
                            <p> The attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                <br />
                                Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                            </p>
                            <h3>Lorem ipsum big title</h3>
                            <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                <br />
                                Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                            </p>
                        </div>
                                            </div>
                </article>
            </div>
            <!--END TOP ARTICLE ONLY MOBILE  MOBILE /MOBILE DETECT SOLUTION-->
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="full-width-col-mobile full-width-col-tablets">
                        <!--TOP ARTICLE TOLONEWS DESKTOP && HIDDEN ON MOBILE -->
                        <div class="top-post-tolonews post-single hidden-onmobile clearfix">
                            <article>
                                <div class="entry-headder">
                                    
                                    <h2 class="grey-dark1 title-top-post-tolonews">
                                        Four Killed, Over 60 Injured in Zabul Suicide Bombing
                                    </h2>
                                </div>
                                                              
                                                               <div class="entry-summary clearfix">
                                    <p> The attack took place in Qalat the capital of the province after a suicide truck bomber detonated his explosives near the provincial council building, deputy police chief, Ghulaam Jelani Farahi said. «Most of the victims are civilians. Women and children were among the injured people» <br />
                                        <br />
                                        Farahi said that the police have launched an investigation into the attack. Officials said that no member of the provincial council has hurt in the attack. The Taliban have claimed responsibility for the attack.
                                    </p>
                                    <h3>Lorem ipsum big title</h3>
                                    <p>
                                        Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. <br />
                                        <br />
                                        Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui.
                                    </p>
                                </div>
                                
                            </article>
                        </div>
                        <!--END TOP ARTICLE TOLONEWS DESKTOP-->
                          </div>
                    
                </div>
            </div>
                                    <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <footer class="row grey-dark">
    <section class="inner-container footer-section">
        <div class="middle">
            <div class="media full-width-col-mobile one-col last-col">
                <a href="#" title=""><img src="images/logo-footer.png" alt="logo" width="170" height="auto" /></a>
                <ul class="social-media">
                    <li>
                        <a href="https://www.facebook.com/TOLOnews" target="_blank" title="Facebook"><i class="fa meduim-play fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="http://www.twitter.com/TOLOnews"  target="_blank" title="Twitter"><i class="fa meduim-play fa-twitter"></i> </a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/+TOLOnews" target="_blank" title="Google+"><i class="fa meduim-play fa-google-plus"></i></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="Youtube"><i class="fa meduim-play fa-youtube-play"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="Instagram"><i class="fa small-picto fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav hidden-onmobile">
                <div class="inline-columnFooter">
                    <h4>TOLOnews</h4>
                    <ul>
                        <li><a href="#" title="Home">Home</a></li>
                        <li><a href="#" title="Politics">Politics</a></li>
                        <li><a href="#" title="World">World</a></li>
                        <li><a href="#" title="Opinion">Opinion</a></li>
                        <li><a href="#" title="Media">Media</a></li>
                        <li><a href="#" title="Art & Culture">Art & Culture</a></li>
                        <li><a href="#" title="Sports">Sports</a></li>
                        <li><a href="#" title="Business">Business</a></li>
                    </ul>
                </div>
                <div class="inline-columnFooter last">
                    <h4>Elections</h4>
                    <ul>
                        <li><a href="#" title="Home">Home</a></li>
                        <li><a href="#" title="News">News</a></li>
                        <li><a href="#" title="Candidates">Candidates</a></li>
                        <li><a href="#" title="Debates & Analysis">Debates & Analysis</a></li>
                        <li><a href="#" title="Provincial">Provincial</a></li>
                        <li><a href="#" title="Videos">Videos</a></li>
                        <li><a href="#" title="FAQ">FAQ</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-newsletter full-width-col-mobile">
                <h4>Newsletter</h4>
                <p>Sign up to receive the bestof Tolo News daily</p>
                <form method="post" action="#">
                    <label for="email" class="hide">Your email address </label>
                    <input id="email" type="email"
                        name="email" placeholder="Your email address">
                    <input type="submit"
                        value="ok">
                </form>
                <ul class="post-numbers hidden-onmobile">
                    <li><span class="numbers">3057</span><span> articles</span>
                    </li>
                    <li><span class="numbers">23864</span><span> comments</span>
                    </li>
                    <li><span class="numbers">957400</span><span>followers</span>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav-copyright clearfix">
            <li class="one-col full-width-col-mobile">©2014 Tolo News - All Rights
                Reserved
            </li>
            <li class="one-col full-width-col-mobile">
                <ul class="nav">
                    <li><a href="#" title="About us">About us</a></li>
                    <li><a href="#" title="Terms of Use">Terms of Use</a></li>
                    <li><a href="#" title="Advertise with us">Advertise with us</a></li>
                    <li><a href="#" title="Contact">Contact</a></li>
                </ul>
            </li>
        </ul>
    </section>
</footer>
        </div>
    </body>
</html>
