<!DOCTYPE html>
<html lang="en">
    <head>
        <title>طلوع نیوز</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- iOS touch icon -->
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png">
        <!--
            ________________
            / ____/ ___/ ___/
            / /    \__ \\__ \
            / /___ ___/ /__/ /
            \____//____/____/
            ======================= -->
        <!-- <link rel="stylesheet" type="text/css" href="css/common.css">
            <link rel="stylesheet" type="text/css" href="css/tv-program.css">
            <link rel="stylesheet" type="text/css" href="css/min.css">-->
        <!--  include CSS / JS  common-->
        <?php include( "php/includes/inc_css_js.php"); ?>
        <link rel="stylesheet" href="css/rtl-app.css">
    </head>
    <body class="is-exiting">
        <!--    __  __               __
            / / / /__  ____ _____/ /__  _____
            / /_/ / _ \/ __ `/ __  / _ \/ ___/
            / __  /  __/ /_/ / /_/ /  __/ /
            /_/ /_/\___/\__,_/\__,_/\___/_/
            ======================================= -->
        <?php include 'php/includes/header-af.php'; ?>
        <div id="wrapper" class="wrapper dark-tv-pages row clearfix">
            <div class="row">
                <div class="inner-container">
                    <h1 class="white category-title">برنامه تلویزیون</h1>
                    <div class="two-col full-width-col-tablets full-width-col-mobile">
                        <div class="content-watch-tolo clearfix tv-program">
                            <!--video watch-->
                            <div class="video-content">
                                <iframe width="650" height="365" src="https://www.youtube.com/embed/2L_WTN4OOqc" allowfullscreen=""></iframe>
                            </div>
                            <!--end-->
                            <div class="info">
                                <h3>ستاره افغان فصل 10 - بزرگ آخر - جواد حسن زاده</h3>
                                <span class="post-date grey-light3">
                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                            </div>
                        </div>
                        <!-- VIDEO PROGRAM-->
                        <div class="video-program-list">
                            <div class="row-video clearfix">
                                <div class="container-top-post line line-grey2">
                                    <h2 class="title-category white uppercase-txt">جعبه سوال</h2>
                                    <a title="View all Business news" href="#" class="view-all-post grey-light5">مشاهده فیلم های بیشتر</a>
                                </div>
                                <ul class="three-col">
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews last-col one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row-video clearfix">
                                <div class="container-top-post line line-grey2">
                                    <h2 class="title-category white uppercase-txt">قسمت کامل</h2>
                                    <a title="View all Business news" href="#" class="view-all-post grey-light5">مشاهده فیلم های بیشتر</a>
                                </div>
                                <ul class="three-col">
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews last-col one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row-video clearfix">
                                <div class="container-top-post line line-grey2">
                                    <h2 class="title-category white uppercase-txt">پشت صحنه</h2>
                                    <a title="View all Business news" href="#" class="view-all-post grey-light5">مشاهده فیلم های بیشتر</a>
                                </div>
                                <ul class="three-col">
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews last-col one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row-video clearfix">
                                <div class="container-top-post line line-grey2">
                                    <h2 class="title-category white uppercase-txt">نماهنگ</h2>
                                    <a title="View all Business news" href="#" class="view-all-post grey-light5">مشاهده فیلم های بیشتر</a>
                                </div>
                                <ul class="three-col">
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews last-col one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row-video clearfix">
                                <div class="container-top-post line line-grey2">
                                    <h2 class="title-category white uppercase-txt">وله</h2>
                                    <a title="View all Business news" href="#" class="view-all-post grey-light5">مشاهده فیلم های بیشتر</a>
                                </div>
                                <ul class="three-col">
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video3.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="post-tolonews last-col one-col">
                                        <a href="#" title="">
                                            <div class="container-image video-content">
                                                <img src="images/image-video2.jpg" alt="" width="206" height="137">
                                                <div class="play"><i class="fa fa-play"></i> </div>
                                            </div>
                                            <div class="content-article">
                                                <h4>
                                                    تیم ملی فوتبال سر در خارج از کشور به جام جهانی مقدماتی
                                                </h4>
                                                <span class="post-date grey-light3">
                                                <span class="uppercase-txt">امروز</span> - ۰۳:۰۰ بعد از ظهر </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- END VIDEO PROGRAM-->
                    </div>
                    <div class="fullWidth-pub  marginRow hidden-onmobile show-onlytablets hidden centered">
                        <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90"> </a>
                    </div>
                    <div class="one-col full-width-col-tablets full-width-col-mobile last-col sidebarRight">
                        <!--SOCIAL SHARE-->
                        <div class="social-sidebar twoo-column-tablets hidden-onmobile clearfix">
                            <h3 class="line line-grey3 white uppercase-txt">ما را دنبال کنید </h3>
                            <div class="social-content post-tolonews">
                                <div class="box-social">
                                    <h3 class="title-article white">ما در شبکه های اجتماعی عضویت</h3>
                                    <ul class="social-media">
                                        <li>
                                            <a href="https://www.facebook.com/TOLOnews" target="_blank" title="فیس بوک"><i class="fa meduim-play fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://www.twitter.com/TOLOnews" target="_blank" title="توئیتر"><i class="fa meduim-play fa-twitter"></i> </a>
                                        </li>
                                        <li>
                                            <a href="https://plus.google.com/+TOLOnews" target="_blank" title="گوگل پلاس"><i class="fa meduim-play fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.youtube.com/user/TOLOnewsLive" target="_blank" title="یوتیوب"><i class="fa meduim-play fa-youtube-play"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/tolonewsOfficial/" target="_blank" title="اینستاگرام"><i class="fa small-picto fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                    <div class="col-newsletter full-width-col-mobile">
                                        <h3 class="title-article white">خبرنامه</h3>
                                        <p>ثبت نام برای دریافت بهترین طلوع نیوز روزانه</p>
                                        <form action="#" method="post">
                                            <label class="hide" for="email">آدرس ایمیل شما </label>
                                            <input type="email" placeholder="آدرس ایمیل شما" name="email" id="email">
                                            <input type="submit" value="موافقم">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--SOCIAL SHARE-->
                        <!--PUB-->
                        <div class="box pub equal-top-pub twoo-column-tablets last">
                            <a href="#" title=""> <img src="images/pub.jpg" alt="" width="300" height="250" /> </a>
                        </div>
                        <!--END PUB-->
                        <div class="box-fb-timeline hidden-ontablets hidden-onmobile equal-top-pub">
                            <div class="container-top-post no-margin line line-grey">
                                <h3 class="titleBlock white uppercase-txt">ما را در فیسبوک پیدا کنید</h3>
                                <a  target="_blank" href="https://www.facebook.com/tolonews"  title="Tolonews" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="fb-page" data-href="https://www.facebook.com/tolonews" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/tolonews"><a href="https://www.facebook.com/tolonews">طلوع نیوز</a></blockquote>
                                </div>
                            </div>
                        </div>
                        <!--PUB-->
                        <div class="box equal-top-pub box-tolo-twitter hidden-onmobile  hidden-ontablets last">
                            <div class="container-top-post no-margin line line-grey">
                                <h3 class="titleBlock white uppercase-txt">توئیتر طلوع نیوز</h3>
                                <a title="Tolonews" href="https://twitter.com/TOLOnews" target="_blank" class="grey-light5 grey-light4 view-all-post">@Tolonews</a>
                            </div>
                            <div class="twitter-feed">
                                <a
                                    data-link-color="#cfcfcf"
                                    data-chrome="nofooter noscrollbar  transparent noheader "
                                    data-src="false"
                                    data-tweet-limit="4"
                                    data-theme="dark"
                                    data-border-color="#fff"
                                    data-show-replies="false"
                                    class="twitter-timeline" href="https://twitter.com/TOLOnews" data-widget-id="673826742377803776">Tweets de @TOLOnews</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                <!--Follow @TOLOnews-->
                                <div class="centered follow-tolo"> <a  href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2F127.0.0.1%2Ftolonewsfinal%2Fpage-section.php&ref_src=twsrc%5Etfw&region=follow_link&screen_name=TOLOnews&tw_p=followbutton" class="white" target="back"> <i class="fa fa-twitter"></i> <span>را دنبال کنید @TOLOnews</span> </a></div>
                            </div>
                        </div>
                        <!--END PUB-->
                    </div>
                    <div class="fullWidth-pub  marginRow hidden-onmobile hidden-ontablets centered">
                        <a href="#" title=""> <img src="images/pub3.jpg" alt="pub" width="728" height="90"> </a>
                    </div>
                </div>
            </div>
            <!--
                ______            __
                / ____/___  ____  / /____  _____
                / /_  / __ \/ __ \/ __/ _ \/ ___/
                / __/ / /_/ / /_/ / /_/  __/ /
                /_/    \____/\____/\__/\___/_/
                ======================================= -->
            <?php include 'php/includes/footer-af.php'; ?>
        </div>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>
