<?php

/**
 * @file
 * Contains \Drupal\purge\Plugin\Purge\Purger\Exception\CapacityException.
 */

namespace Drupal\purge\Plugin\Purge\Purger\Exception;

/**
 * Thrown when system resources are reached or exceeded.
 */
class CapacityException extends \Exception {}
