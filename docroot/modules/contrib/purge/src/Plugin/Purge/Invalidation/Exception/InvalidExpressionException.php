<?php

/**
 * @file
 * Contains \Drupal\purge\Plugin\Purge\Invalidation\Exception\InvalidExpressionException.
 */

namespace Drupal\purge\Plugin\Purge\Invalidation\Exception;

/**
 * Thrown when invalidations are instantiated with a incorrect expression.
 */
class InvalidExpressionException extends \Exception {}
