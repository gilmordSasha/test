<?php

/**
 * @file
 * Contains \Drupal\purge\Plugin\Purge\Invalidation\Exception\MissingExpressionException.
 */

namespace Drupal\purge\Plugin\Purge\Invalidation\Exception;

/**
 * Thrown when invalidations are instantiated without required expression.
 */
class MissingExpressionException extends \Exception {}
