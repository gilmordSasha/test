<?php

/**
 * @file
 * Contains Drupal\tolonews_schedule\TolonewsScheduleServiceProvider
 */

namespace Drupal\tolonews_schedule;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class TolonewsScheduleServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Override field manager for the scheduled update fields.
    $definition = $container->getDefinition('scheduled_updates.field_manager');
    $definition->setClass('Drupal\tolonews_schedule\ToloNewsFieldManager');
    $definition = $container->getDefinition('workbench_moderation.entity_operations');
    $definition->setClass('Drupal\tolonews_schedule\ToloNewsEntityOperations');
    $definition = $container->getDefinition('workbench_moderation.revision_tracker');
    $definition->setClass('Drupal\tolonews_schedule\ToloNewsRevisionTracker');
  }
}
