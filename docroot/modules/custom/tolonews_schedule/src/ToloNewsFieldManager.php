<?php

/**
 * @file
 * contains Drupal\tolonews_schedule\ToloNewsFieldManager
 */

namespace Drupal\tolonews_schedule;
use Drupal\scheduled_updates\FieldManager;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\scheduled_updates\ScheduledUpdateTypeInterface;

class ToloNewsFieldManager extends FieldManager {
  /**
   * [@inheritdoc}
   */
  public function cloneField(ScheduledUpdateTypeInterface $scheduled_update_type, $field_name, $field_config_id = NULL, array $default_value = [], $hide = FALSE) {
    $entity_type = $scheduled_update_type->getUpdateEntityType();
    $definition = $this->getFieldStorageDefinition($entity_type, $field_name);
    if (!$definition) {
      return FALSE;
    }

    $new_field_name = $this->getNewFieldName($definition);
    $field_storage_values = [
      'field_name' => $new_field_name,
      'entity_type' => 'scheduled_update',
      'type' => $definition->getType(),
      'translatable' => $definition->isTranslatable(),
      'settings' => $definition->getSettings(),
      'cardinality' => $definition->getCardinality(),
      // 'module' => $definition->get @todo how to get module
    ];
    $field_values = [
      'field_name' => $new_field_name,
      'entity_type' => 'scheduled_update',
      'bundle' => $scheduled_update_type->id(),
      'label' => $definition->getLabel(),
      // Field translatability should be explicitly enabled by the users.
      'translatable' => TRUE,
    ];

    /** @var FieldConfig $field_config */
    if ($field_config_id && $field_config = FieldConfig::load($field_config_id)) {
      $field_values['settings'] = $field_config->getSettings();
      $field_values['label'] = $field_config->label();
    }

    // @todo Add Form display settings!

    FieldStorageConfig::create($field_storage_values)->save();
    /** @var FieldConfigBase $field */
    $field = FieldConfig::create($field_values);
    $field->save();
    if ($default_value) {
      $field->setDefaultValue($default_value);
      $field->save();
    }
    if (!$hide) {
      $this->createFormDisplay($scheduled_update_type, $field_config_id, $definition, $new_field_name);
    }

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function createNewReferenceField(array $new_field_settings, ScheduledUpdateTypeInterface $scheduled_update_type) {
    $entity_type = $scheduled_update_type->getUpdateEntityType();
    $field_name = $this->createNonExistingFieldName($new_field_settings['field_name'], $entity_type);
    $label = $new_field_settings['label'];
    if ($new_field_settings['cardinality'] == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $new_field_settings['cardinality_number'] = FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED;
    }
    $field_storage_values = [
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => 'entity_reference',
      'translatable' => TRUE,
      'settings' => ['target_type' => 'scheduled_update'],
      'cardinality' => $new_field_settings['cardinality_number'],
      // @todo Add config to form
      // 'module' => $definition->get @todo how to get module
    ];
    FieldStorageConfig::create($field_storage_values)->save();
    $bundles = array_filter($new_field_settings['bundles']);
    foreach ($bundles as $bundle) {
      $field_values = [
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'label' => $label,
        // Field translatability should be explicitly enabled by the users.
        'translatable' => TRUE,
        'settings' => [
          'handler_settings' => [
            'target_bundles' => [$scheduled_update_type->id()],
          ],
        ],
      ];
      $field = FieldConfig::create($field_values);
      $field->save();
      $this->addToDefaultFormDisplay($entity_type, $bundle, $label, $field_name);


    }
  }
}
