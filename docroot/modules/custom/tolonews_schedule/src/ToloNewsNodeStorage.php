<?php

/**
 * @file
 * Contains \Drupal\tolonews_schedule\ToloNewsNodeStorage
 */

namespace Drupal\tolonews_schedule;
use Drupal\node;
use Drupal\node\NodeStorage;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the storage handler class for nodes.
 */
class ToloNewsNodeStorage extends NodeStorage {
  /**
   * {@inheritdoc}
   */
  protected function saveToSharedTables(ContentEntityInterface $entity, $table_name = NULL, $new_revision = NULL) {
    $active_langcode = $entity->language()->getId();
    if (!isset($table_name)) {
      $table_name = $this->dataTable;
    }
    if (!isset($new_revision)) {
      $new_revision = $entity->isNewRevision();
    }
    $revision = $table_name != $this->dataTable;

    if (!$revision || !$new_revision) {
      $key = $revision ? $this->revisionKey : $this->idKey;
      $value = $revision ? $entity->getRevisionId() : $entity->id();
      // Delete and insert to handle removed values.
      $this->database->delete($table_name)
        ->condition($key, $value)
        ->execute();
    }

    $query = $this->database->insert($table_name);
    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      $translation = $entity->getTranslation($langcode);
      // @see https://www.drupal.org/node/2766957
      // Here we check if entity we store is in the default moderation state and
      // at the same time translation is in the non-default state.
      // If both conditions pass then we look for the last default-state revision
      // and build the database record based on it.
      if ($active_langcode != $langcode && isset($translation->moderation_state)) {
        if ($entity->isDefaultRevision() && !$translation->moderation_state->entity->isDefaultRevisionState()) {
          if ($revision_translation = $this->getLatestPublishedRevision($entity, $langcode)) {
            // This will obviously lead to the loose of the revision translation. 
            // That's why we schedule that revision re-save upon the request end. 
            $this->scheduleRestoreLatestRevision($entity);
            $translation = $revision_translation;
          }
        }
        // This code runs on the script shutdown.
        // Here we restore the revision which was lost on the default state save.
        // 'get_latest_revisions' property is the recursion protection set in the tolonews_schedule_node_resave().
        if (!empty($entity->get_latest_revisions)) {
          if ($revision_translation = $this->getLatestRevision($entity, $langcode)) {
            $translation = $revision_translation;
          }
        }
      }
      $record = $this->mapToDataStorageRecord($translation, $table_name);
      $values = (array) $record;
      $query
        ->fields(array_keys($values))
        ->values($values);
    }

    $query->execute();
  }

  /**
   * Look for the latest published revision for the given language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param $langcode
   * @return mixed
   */
  protected function getLatestPublishedRevision(ContentEntityInterface $entity, $langcode) {
    $revision_query = \Drupal::entityQuery('node');
    $revision_query->condition('nid', $entity->id());
    // @todo query for the default states.
    $revision_query->condition('moderation_state', array('published', 'unpublised'), 'IN', $langcode);
    $revision_query->sort('vid', 'DESC');
    $revision_query->allRevisions();
    $revision_query->range(0, 1);
    $result = $revision_query->execute();
    $vid = key($result);
    if ($vid && $vid != $entity->getRevisionId()) {
      $revision_translation = node_revision_load($vid);
      $revision_translation = $revision_translation->getTranslation($langcode);
      $revision_translation->nid = $entity->id();
      $revision_translation->vid = $entity->getRevisionId();
      return $revision_translation;
    }
    return FALSE;
  }

  /**
   * Get the latest revision from the workbecnh_moderation tracker.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param $langcode
   * @return bool
   */
  protected function getLatestRevision(ContentEntityInterface $entity, $langcode) {
    // @todo inject.
    // @see ModerationInformation::getLatestRevisionId - it does not looks for the 
    $tracker = \Drupal::service('workbench_moderation.revision_tracker');
    $latest_revision_id = $tracker->getLatestRevision('node', $entity->id(), $langcode);
    if ($latest_revision_id) {
      $revision_translation = node_revision_load($latest_revision_id);
      $revision_translation = $revision_translation->getTranslation($langcode);
      $revision_translation->nid = $entity->id();
      $revision_translation->vid = $entity->getRevisionId();
      return $revision_translation;
    }
    return FALSE;
  }

  /**
   * Set entity for the resave on the request end.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  protected function scheduleRestoreLatestRevision(ContentEntityInterface $entity) {
    if (empty($entity->restore_revision)) {
      $resave = clone $entity;
      $resave->isDefaultRevision(FALSE);
      // This flag will be used in the ToloNewsEntityOperations::entityPresave() for determining the default state of the revision.
      $resave->restore_revision = TRUE;
      drupal_register_shutdown_function('\tolonews_schedule_node_resave', $resave);
    }
  }
}
