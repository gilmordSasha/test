<?php

/**
 * @file
 * 
 * contains \Drupal\tolonews_schedule\Plugin\UpdateRunner\MultiLingualUpdateRunner
 */

namespace Drupal\tolonews_schedule\Plugin\UpdateRunner;
use Drupal\scheduled_updates\Plugin\UpdateRunner\LatestRevisionUpdateRunner;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\scheduled_updates\ScheduledUpdateInterface;
use Drupal\scheduled_updates\Plugin\UpdateRunnerInterface;

/**
 * Latest revision update runner with translatable content support.
 *
 * @UpdateRunner(
 *   id = "multi_lingual",
 *   label = @Translation("Multilingual Revisions"),
 *   update_types = {"embedded"},
 *   description = @Translation("Runs updates always against the latest revision of revisionable entities content with respect to translations.")
 * )
 */
class MultiLingualUpdateRunner extends LatestRevisionUpdateRunner {

  /**
   * {@inheritdoc}
   */
  protected function runUpdate(ScheduledUpdateInterface $update, $entity_ids, $queue_item) {
    /** @var ContentEntityInterface[] $entities_to_update */
    $entities_to_update = $this->loadEntitiesToUpdate($entity_ids);

    $invalid_entity_ids = [];
    foreach (array_keys($entities_to_update) as $entity_id) {
      $entity_to_update = $entities_to_update[$entity_id];
      if (!empty($queue_item->data['langcode'])) {
        $entity_to_update = $entity_to_update->getTranslation($queue_item->data['langcode']);
      }
      $this->prepareEntityForUpdate($update, $queue_item, $entity_to_update);
      $this->switchUser($update, $entity_to_update);
      $violations = $entity_to_update->validate();
      if (count($violations)) {
        $invalid_entity_ids[] = $entity_id;
      }
      else {
        // Validation was successful.
        $entity_to_update->save();
      }
      $this->switchUserBack();
    }

    // @todo Should an update only be consider successfull if all entites were update correctly.
    // @todo Should all entities should be rolled back if 1 can't be updated? Add an option?
    $successful = empty($invalid_entity_ids);
    if (!$successful) {
      // At least 1 entity could not be updated.
      if ($this->getInvalidUpdateBehavior() == UpdateRunnerInterface::INVALID_REQUEUE) {
        // We can't release the item now or it will be claimed again.
        $update->status = ScheduledUpdateInterface::STATUS_REQUEUED;
        if ($this->scheduled_update_type->isEmbeddedType()) {
          $update->setUpdateEntityIds($invalid_entity_ids);
        }
        $update->save();
        $this->addItemToRelease($queue_item);
      }
      elseif ($this->getInvalidUpdateBehavior() == UpdateRunnerInterface::INVALID_ARCHIVE) {
        // @todo Should the successful entities be removed
        $update->status = ScheduledUpdateInterface::STATUS_UNSUCESSFUL;
        $update->save();
        $this->getQueue()->deleteItem($queue_item);
      }
      else {
        $update->delete();
        $this->getQueue()->deleteItem($queue_item);
      }
    }
    else {
      // There were no invalid entity updates
      if ($this->getAfterRun() == UpdateRunnerInterface::AFTER_DELETE) {
        $update->delete();
      }
      else {
        $update->status = ScheduledUpdateInterface::STATUS_SUCCESSFUL;
        $update->save();
      }
      $this->getQueue()->deleteItem($queue_item);
    }
    return $successful;
  }

  /**
   * {@inheritdoc}
   */
  protected function loadEntitiesToUpdate($entity_ids) {
    $revision_ids = array_keys($entity_ids);
    $entity_ids = array_unique($entity_ids);
    $revisions = [];
    foreach ($entity_ids as $entity_id) {
      /** @var ContentEntityInterface $latest_revision */
      $latest_revision = $this->updateUtils->getLatestRevision($this->updateEntityType(), $entity_id);
      // Check the latest revision was in the revisions sent to this function.
      if (in_array($latest_revision->getRevisionId(), $revision_ids)) {
        $revisions[$entity_id] = $latest_revision;
      }
    }
    return $revisions;
  }

  /**
   * Return all schedule updates that are referenced via Entity Reference
   * fields.
   *
   * @return ScheduledUpdate[]
   */
  protected function getEmbeddedUpdates() {
    $updates = [];
    /** @var String[] $fields */
    if ($entity_ids = $this->getEntityIdsReferencingReadyUpdates()) {
      if ($entities = $this->loadEntitiesToUpdate($entity_ids)) {
        $field_ids = $this->getReferencingFieldIds();
        /** @var ContentEntityInterface $entity */
        foreach ($entities as $entity) {
          $languages = $entity->getTranslationLanguages();
          foreach ($languages as $langcode => $language) {
            $translation = $entity->getTranslation($langcode);

            /** @var  $entity_update_ids - all update ids for this entity for our fields. */
            $entity_update_ids = [];
            /** @var  $field_update_ids - update ids keyed by field_id. */
            $field_update_ids = [];
            foreach ($field_ids as $field_id) {
              // Store with field id.
              $field_update_ids[$field_id] = $this->getEntityReferenceTargetIds($translation, $field_id);
              // Add to all for entity.
              $entity_update_ids += $field_update_ids[$field_id];
            }
            // For all entity updates return only those ready to run.
            $ready_update_ids = $this->getReadyUpdateIds($entity_update_ids);
            // Loop through updates attached to fields.
            foreach ($field_update_ids as $field_id => $update_ids) {
              // For updates attached to field get only those ready to run.
              $field_ready_update_ids = array_intersect($update_ids, $ready_update_ids);
              foreach ($field_ready_update_ids as $field_ready_update_id) {
                $updates[] = [
                  'update_id' => $field_ready_update_id,
                  // If this is revisionable entity use revision id as key for Runner Plugins that care about revisions.
                  'entity_ids' => $translation->getRevisionId()? [$translation->getRevisionId() => $translation->id()]: [$translation->id()],
                  'field_id' => $field_id,
                  'entity_type' => $this->updateEntityType(),
                  'langcode' => $langcode,
                ];
              }
            }
          }
        }
      }
    }
    return $updates;

  }
}
