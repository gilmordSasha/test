<?php

/**
 * @file
 * Contains \Drupal\tolonews_schedule\ToloNewsRevisionTracker
 */
namespace Drupal\tolonews_schedule;

use Drupal\workbench_moderation\RevisionTracker;

class ToloNewsRevisionTracker extends RevisionTracker {

  /**
   * @param $entity_type
   * @param $entity_id
   * @param $langcode
   * @return mixed
   */
  public function getLatestRevision($entity_type, $entity_id, $langcode) {
    return $this->connection->select($this->tableName, 'tracker')
      ->condition('tracker.entity_type', $entity_type)
      ->condition('tracker.entity_id', $entity_id)
      ->condition('tracker.langcode', $langcode)
      ->fields('tracker', array('revision_id'))
      ->execute()
      ->fetchField();
  }

  /**
   * @param $entity_type
   * @param $entity_id
   * @return int
   */
  public function removeTrackedEntity($entity_type, $entity_id) {
    return $this->connection->delete($this->tableName)
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute();
  }
}
