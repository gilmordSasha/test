(function() {
  CKEDITOR.plugins.add("h3", {
    init:function(editor) {
      editor.addCommand("h3", {
        exec: function(editor){
          var format = {
            element : "h3"
          };
          var style = new CKEDITOR.style(format);
          style.apply(editor.document);
        }
      });
      editor.ui.addButton("h3", {
        label: "H3",
        icon: this.path + "/images/h3.png",
        command: "h3"
      });
    }
  });
})();
