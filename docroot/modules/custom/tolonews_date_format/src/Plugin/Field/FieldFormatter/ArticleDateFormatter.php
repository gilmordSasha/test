<?php

/**
 * @file
 * Contains \Drupal\tolonews_date_format\Plugin\Field\FieldFormatter\ArticleDateFormatter.
 */

namespace Drupal\tolonews_date_format\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'article_date_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "article_date_formatter",
 *   label = @Translation("Article date formatter"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class ArticleDateFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // @todo inject this if possible.
    $formatter = \Drupal::service('date.formatter');
    $timestamp = strtotime($item->value);
    // @todo use \Drupal\Core\Datetime\DrupalDateTime.
    $today = $formatter->format(REQUEST_TIME, 'custom', 'Y-m-d');
    $yesterday = $formatter->format(strtotime('-1 day'), 'custom', 'Y-m-d');
    $day = $formatter->format($timestamp, 'custom', 'Y-m-d');
    if ($day == $today || $day == $yesterday) {
      // @todo create time formats in Drupal, don't use 'custom' for the output.
      $text = ($day == $today ? t('Today') : t('Yesterday')) . ' - ' . $formatter->format($timestamp, 'custom', 'g:i A');
    }
    else {
      $text = $formatter->format($timestamp, 'custom', 'd F Y');
    }
    if (\Drupal::languageManager()->getCurrentLanguage()->getId() != 'en') {
      $arabic_indic_digits = array(
        "\xD9\xA0",
        "\xD9\xA1",
        "\xD9\xA2",
        "\xD9\xA3",
        "\xD9\xA4",
        "\xD9\xA5",
        "\xD9\xA6",
        "\xD9\xA7",
        "\xD9\xA8",
        "\xD9\xA9",
      );
      $text = str_replace(array_keys($arabic_indic_digits), $arabic_indic_digits, $text);
    }
    return $text;
  }

}
