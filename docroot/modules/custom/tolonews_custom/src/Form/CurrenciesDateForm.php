<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Form\CurrenciesDateForm.
 */

namespace Drupal\tolonews_custom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Contribute form.
 */
class CurrenciesDateForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'currencies_date_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $saved = \Drupal::config('tolonews_custom.config')
      ->get('currencies_date');

    $default = DrupalDateTime::createFromTimestamp($saved ? $saved : time())
      ->format('Y-m-d');

    $form['date'] = [
      '#type' => 'date',
      '#title' => t('Date'),
      '#default_value' => $default,
      '#required' => TRUE,
      '#date_date_format' => 'd/m/Y',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitted_vals = $form_state->getValues();

    \Drupal::service('config.factory')->getEditable('tolonews_custom.config')
      ->set('currencies_date', strtotime($submitted_vals['date']))->save();
  }
}
