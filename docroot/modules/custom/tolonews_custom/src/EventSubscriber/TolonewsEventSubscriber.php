<?php

namespace Drupal\tolonews_custom\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TolonewsEventSubscriber implements EventSubscriberInterface {

  public function doTestStuff(GetResponseEvent $event) {
    if ($event->getRequest()->query->get('buu')) {
//      $query = \Drupal::entityQuery('node');
//      $query->condition('nid', 251);
//      $query->condition('moderation_state', 'published', '=', 'en');
//      $query->sort('changed', 'DESC');
//      $query->allRevisions();
//      $query->range(0, 1);
//      $result = $query->execute();
//      $vid = key($result);
//      $entity = node_revision_load($vid);
//      $entity->setNewRevision(TRUE);
//      $entity->save();

      $revision = node_revision_load(2139)->getTranslation('en');
      $revision->setNewRevision(TRUE);
      $revision->save();
//      $revision = node_revision_load(2139)->getTranslation('en');
      //$event->setResponse(new RedirectResponse('http://example.com/'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('doTestStuff');
    return $events;
  }

}
