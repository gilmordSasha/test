<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Plugin\Condition\HasChildren.
 */

namespace Drupal\tolonews_custom\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'has Children' condition.
 *
 * @Condition(
 *   id = "has_children",
 *   label = @Translation("Has Children"),
 *   context = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", label = @Translation("Term"))
 *   }
 * )
 *
 */
class HasChildren extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return TRUE;
//    $term = $this->getContextValue('taxonomy_term');
//    if (!$term) {
//      return !$this->isNegated();
//    }
//    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('section', $term->id());
//    if (!empty($tree)) {
//      return TRUE;
//    }
//    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

  }
}
