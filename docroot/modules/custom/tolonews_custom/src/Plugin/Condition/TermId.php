<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Plugin\Condition\TermId.
 */

namespace Drupal\tolonews_custom\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ctools\ConstraintConditionInterface;
use Drupal\Driver\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;

/**
 * Provides a 'Term Id' condition.
 *
 * @Condition(
 *   id = "term_id",
 *   label = @Translation("Term Id"),
 *   context = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", label = @Translation("Term"))
 *   }
 * )
 *
 */
class TermId extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return TRUE;
//    $term = $this->getContextValue('taxonomy_term');
//    if (!$term || !$this->configuration['term_id']) {
//      return !$this->isNegated();
//    }
//    return $term->id() == $this->configuration['term_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
//    $form['term_id'] = array(
//      '#type' => 'textfield',
//      '#title' => $this->t('Term ID'),
//      '#default_value' => $this->configuration['term_id'],
//    );
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['term_id'] = $form_state->getValue('term_id');
//    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
//  public function defaultConfiguration() {
////    return array('term_id' => '') + parent::defaultConfiguration();
//  }
}
