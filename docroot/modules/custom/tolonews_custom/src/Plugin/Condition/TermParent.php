<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Plugin\Condition\TermParent.
 */

namespace Drupal\tolonews_custom\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Term Parent' condition.
 *
 * @Condition(
 *   id = "term_parent",
 *   label = @Translation("Term Parent"),
 *   context = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", label = @Translation("Term"))
 *   }
 * )
 *
 */
class TermParent extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return TRUE;
//    $term = $this->getContextValue('taxonomy_term');
//    if (!$term || !$this->configuration['parent_id']) {
//      return !$this->isNegated();
//    }
//    if ($this->configuration['parent_id'] == $term->id()) {
//      return TRUE;
//    }
//
//    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('section', $this->configuration['parent_id']);
//    if (empty($tree)) {
//      return FALSE;
//    }
//    foreach ($tree as $child) {
//      if ($child->tid == $term->id()) {
//        return TRUE;
//      }
//    }
//    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
//    $form['parent_id'] = array(
//      '#type' => 'textfield',
//      '#title' => $this->t('Parent ID'),
//      '#default_value' => $this->configuration['parent_id'],
//    );
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
//    $this->configuration['parent_id'] = $form_state->getValue('parent_id');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
//  public function defaultConfiguration() {
////    return array('parent_id' => '') + parent::defaultConfiguration();
//  }
}
