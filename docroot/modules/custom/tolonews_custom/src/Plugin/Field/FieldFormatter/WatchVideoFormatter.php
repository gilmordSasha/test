<?php

/**
 * @file
 * Contains \Drupal\tolonews_date_format\Plugin\Field\FieldFormatter\ArticleDateFormatter.
 */

namespace Drupal\tolonews_custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\video_embed_field\Plugin\Field\FieldFormatter;
use Drupal\video_embed_field\ProviderManagerInterface;

/**
 * Plugin implementation of the 'watch_video_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "watch_video_formatter",
 *   label = @Translation("Watch Video Formatter"),
 *   field_types = {
 *     "video_embed_field"
 *   }
 * )
 */
class WatchVideoFormatter extends FieldFormatter\Thumbnail {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $images = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      $provider = $this->providerManager->loadProviderFromInput($item->value);
      $elements[$delta] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['container-video-cover']],
        'video' => [
          '#type' => 'container',
          '#attributes' => ['class' => ['video-hidden', 'hidden']],
          'iframe' => $provider->renderEmbedCode('650', '365', FALSE),
        ],
        'image' => [
          '#type' => 'container',
          '#attributes' => ['class' => ['block-video-play', 'video']],
          'image' => $images[$delta],
          'icon' => [
            '#type' => 'html_tag',
            '#tag' => 'i',
            '#value' => '',
            '#attributes' => ['class' => ['big-play', 'fa', 'fa-play', 'play']]
          ],
        ]
      ];
    }

    return $elements;
  }

}
