<?php

namespace Drupal\tolonews_custom\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'first_paragraph' formatter.
 *
 * @FieldFormatter(
 *   id = "first_paragraph",
 *   label = @Translation("First paragraph"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class FirstParagraph extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $text = strip_tags(Html::decodeEntities($item->value));
    $text = array_filter(array_map([$this, 'trim'], explode("\n", $text)));
    return Html::escape(reset($text));
  }

  /**
   * Trim &npbsp;
   * @todo static maybe?
   */
  protected function trim($item) {
    return trim($item, " \t\n\r\0\x0B\xC2\xA0");
  }

}
