<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Plugin\Block\WeatherBlock.
 */

namespace Drupal\tolonews_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides accuweather widget block.
 *
 * @Block(
 *   id = "tolonews_custom_weather_block",
 *   admin_label = @Translation("Weather Block"),
 *   category = @Translation("Tolonews")
 * )
 */
class WeatherBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    list($lang_1, $lang_2) = self::getCurrentLangWeather();

    return [
      '#theme' => 'tolonews_custom_weather',
      '#lang_1' => $lang_1,
      '#lang_2' => $lang_2,
    ];
  }

  /**
   * Returns language params for accuweather widget.
   *
   * @return array
   */
  public static function getCurrentLangWeather() {
    $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();

    switch ($lang_code) {
      case 'pa':
      case 'fa':
        return ['fa', 'af'];

      default:
      case 'en':
        return ['en', 'en'];
    }
  }

}
