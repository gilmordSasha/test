<?php

namespace Drupal\tolonews_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Twitter feed' Block
 *
 * @Block(
 *   id = "twitter_feed",
 *   admin_label = @Translation("Twitter feed"),
 *   category = @Translation("Tolonews")
 * )
 */
class TwitterFeed extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#theme' => 'tolonews_custom_twitter',
    );
  }
}