<?php

namespace Drupal\tolonews_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Social links' Block
 *
 * @Block(
 *   id = "social_links",
 *   admin_label = @Translation("Social links"),
 *   category = @Translation("Tolonews")
 * )
 */
class SocialLinks extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#theme' => 'tolonews_custom_social',
    );
  }
}