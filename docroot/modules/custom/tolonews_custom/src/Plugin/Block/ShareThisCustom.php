<?php

/**
 * @file
 * Contains \Drupal\tolonews_custom\Plugin\Block\WeatherBlock.
 */

namespace Drupal\tolonews_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides accuweather widget block.
 *
 * @Block(
 *   id = "tolonews_custom_share_this_block",
 *   admin_label = @Translation("ShareThis"),
 *   category = @Translation("Tolonews")
 * )
 */
class ShareThisCustom extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // ToDo: real comments count.
    $comments_count = 0;

    return [
      '#theme' => 'tolonews_custom_share_this',
      '#comments_count' => $comments_count,
      '#attached' => [
        'library' =>  [
          'tolonews_custom/sharethis',
        ],
      ],
    ];
  }

}
